<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/api/trajects', [DashController::class, 'trajects']);
Route::get('/api/trajects/{id}', [DashController::class, 'traject']);
Route::get('/api/trajects/{id}/procedure', [DashController::class, 'procedure']);
