<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

use App\Models\Trajects;

class DashController extends Controller
{
    // Traject state filters.
    private function filterActive($query) { return $query->whereIn('base_id', [3, 4, 5, 6]); }
    private function filterPreActive($query) { return $query->whereIn('base_id', [2]); }
    private function filterFinished($query) { return $query->whereIn('base_id', [7, 20]); }
    private function filterNotFinished($query) { return $query->whereNotIn('base_id', [7, 20]); }
    private function filterState($query, $state) 
    {
        switch($state) 
        {
            case 'active': $query = $this->filterActive($query); break;
            case 'pre_active': $query = $this->filterNotLongterm($this->filterPreActive($query)); break; // Only if not longterm
            case 'finished': $query = $this->filterFinished($query); break;
            case 'not_finished': $query = $this->filterNotFinished($query); break;
        }
        return $query;
    }

    // Other filters
    private function filterRisks($query) { return $query->whereDate('start_contract', '<=', Carbon::now('Europe/Amsterdam')); }
    private function filterLongterm($query) { return $query->whereDate('start_contract', '>', Carbon::now('Europe/Amsterdam')->addYears(1)); }
    private function filterNotLongterm($query) { return $query->whereDate('start_contract', '<=', Carbon::now('Europe/Amsterdam')->addYears(1)); }

    private function filterPhases($query, $phases) 
    {
        $p = explode(',', $phases);
        return $query->whereIn('base_id', $p);
    }

    private function filterBuyers($query, $buyers) 
    {
        $p = explode(',', $buyers);
        return $query->whereIn('buyer_id', $p);
    }

    private function filterProcedures($query, $procedures) 
    {
        $p = explode(',', $procedures);
        return $query->whereIn('procedure_id', $p);
    }

    // Merge with the eloquent relationships
    private function mergeWithTables($statefilter, $phasefilter) 
    {
        // Use with to get the relationships
        return Trajects::with(['procedure:id,name,lead_period,buyer_hours,publication_phase,assessment_phase,objection_phase', 
            'phase:id,name,base_id,base_name', 'buyer:id,first_name,last_name'])

        // Filter the phases
        ->whereHas('phase', function($query) use(&$statefilter, &$phasefilter) 
        {
            // Predefined states like active/finished etc.
            if (isset($statefilter))
                $query = $this->filterState($query, $statefilter);

            // Explicit phase ids
            if (isset($phasefilter))
                $query = $this->filterPhases($query, $phasefilter);
        });
    }

    public function trajects() 
    {
        // Validate the attributes, even ones without constraints, so they get added to 
        // the $attributes variable if they exist.
        $attributes = request()->validate([
            'state' => 'in:active,pre_active,finished,not_finished',
            'longterm' => '',
            'phases' => '',
            'buyers' => '',
            'procedures' => '',
            'risk' => '',
        ]);

        // When we want risk trajects, the state must be 'not_finished'
        if (isset($attributes['risk'])) 
            $attributes['state'] = 'not_finished';

        // Since the state is part of the traject_statuses table, we need to filter when
        // merging with the table. So we send the attributes to this function.
        $state = null;
        $phases = null;
        if (isset($attributes['state'])) $state = $attributes['state'];
        if (isset($attributes['phases'])) $phases = $attributes['phases'];
        $query = $this->mergeWithTables($state, $phases);

        // Other filters
        if (isset($attributes['risk'])) 
            $query = $this->filterRisks($query);

        if (isset($attributes['longterm'])) 
            $query = $this->filterLongterm($query);

        if (isset($attributes['buyers'])) 
            $query = $this->filterBuyers($query, $attributes['buyers']);

        if (isset($attributes['procedures'])) 
            $query = $this->filterProcedures($query, $attributes['procedures']);

        return $query->get();
    }
        
    public function traject($id) 
    {
        $traject = App\Models\Trajects::findOrFail($id);
    
        return $traject;
    }
    
    public function procedure($id) 
    {
        return traject($id)->procedure;
    }
}
