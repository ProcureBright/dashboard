<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Procedure;
use App\Models\TrajectPhase;
use App\Models\User;

class Trajects extends Model
{
    use HasFactory;

    protected $table = 'trajects';

    public function procedure() 
    {
        return $this->hasOne(Procedure::class, 'id', 'procedure_id');
    }

    public function phase() 
    {
        return $this->hasOne(TrajectPhase::class, 'id', 'status_id');
    }

    public function buyer() 
    {
        return $this->hasOne(User::class, 'id', 'buyer_id');
    }
}
