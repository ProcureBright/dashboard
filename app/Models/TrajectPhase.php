<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TrajectPhase extends Model
{
    use HasFactory;

    protected $table = 'traject_phases';
}
