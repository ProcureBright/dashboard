// Settings enum for the charts
window.ChartSettings = Object.freeze({
    ShowCount: 1, ShowBuyerHours: 2, ShowAverageLeadPeriod: 3, ShowAverageBuyerHours: 4,
    ForEachMonth: 10, ForEachPhase: 11, 
    PerProcedure: 20, PerBuyer: 21, PerPhase: 22,
    NmrRunningTrajects: 30, NmrTrajectsToStart: 31, NmrFinishedTrajects: 32, 
    NmrRiskTrajects: 33, NmrLongtermTrajects: 34, NmrOnHoldTrajects: 35, NmrWithdrawnTrajects: 36,
});

// Colors used by apexchart
window.Colors = [
    "#008FFB","#00E396","#FEB019","#FF4560","#775DD0","#3F51B5",
    "#03A9F4","#4CAF50","#F9CE1D","#FF9800","#33B2DF","#546E7A",
    "#D4526E","#13D8AA","#A5978B","#4ECDC4","#C7F464","#81D4FA",
    "#546E7A","#FD6A6A","#2B908F","#F9A3A4","#90EE7E","#FA4443",
    "#69D2E7","#449DD1","#F86624","#EA3546","#662E9B","#C5D86D",
    "#D7263D","#1B998B","#2E294E","#F46036","#E2C044","#662E9B",
    "#F86624","#F9C80E","#EA3546","#43BCCD","#5C4742","#A5978B",
    "#8D5B4C","#5A2A27","#C4BBAF" 
];

// Order of all the phases
const PhaseIndex = { 
    'Nog op te starten': 0,
    'Voorbereiden': 1,
    'Publicatiefase': 2,
    'Beoordelingsfase': 3,
    'Gunningsfase/Standstill': 4,
    'Afgerond': 5,
    'On Hold': 6,
    'Teruggetrokken': 7,
    'Onbekend': 8,
}

// Some phases are grouped/have a name alias. 
function getPhaseAlias(name) { 
    if (name == 'Idee') return 'Nog op te starten';
    if (name == 'Koppelen inkoper') return 'Nog op te starten';
    if (name == 'Gepubliceerd/loopt') return 'Publicatiefase';
    if (name == 'Gunningsfase en standstill') return 'Gunningsfase/Standstill';
    return name;
}

// x-axis filtering
// divides all the trajects in 'result' into 
// groups to divide along the x-axis.
function filterPerMonth(result, settings, chart) {
    const monthNames = ["Jan", "Feb", "Maa", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec" ];

    let periods = [], categories = [], period = new Date(); 
    period.setDate(0) // Set to first day of the month

    // When looking into the future, start 'settings.count' months ahead
    if (settings.count > 0) period.setMonth(period.getMonth() + settings.count);
    
    for (let i = 0; i < Math.abs(settings.count); i++) {

        // Create entry for this month
        // and go through all the trajects to check if they are in the current month
        periods.push({ trajects: [] });
        result.data.forEach(traject => {
            if (traject.start_contract) { // Only continue if a start has been defined.

                // Parse the traject start and end
                let traject_end = new Date(Date.parse(traject.start_contract));
                let traject_start = new Date(Date.parse(traject.start_contract));
                traject_start.setMonth(traject_end.getMonth() - traject.time);
                
                // If no filter for phases check if traject started before this period, and lasts at least till this period
                if (chart.selectedPhases.length == 0) {
                    if (traject_start <= period && traject_end >= period)
                        periods[periods.length - 1].trajects.push(traject);
                } else {

                    // The amount of days per phase is defined in the procedure of the traject
                    const phase1 = traject.procedure.publication_phase;
                    const phase2 = traject.procedure.assessment_phase;
                    const phase3 = traject.procedure.objection_phase;

                    let add = false; // Determine wether we should add this traject, given the phase filter
                    chart.selectedPhases.forEach(phase => {
                        let start = new Date(traject_end.getTime()), end = new Date(traject_end.getTime());
                        switch(phase.id) {
                        case 0: // Voorbereidingsfase, starts at traject start, ends at end minus all other phases
                            start = new Date(traject_start.getTime());
                            end.setDate(end.getDate() - phase3 - phase2 - phase1);
                        case 1: // Publicatiefase
                            start.setDate(start.getDate() - phase3 - phase2 - phase1);
                            end.setDate(end.getDate() - phase2 - phase1);
                        case 2: // Beoordelingsfase
                            start.setDate(start.getDate() - phase2 - phase1);
                            end.setDate(end.getDate() - phase1);
                        case 3: // Bezwaartermijn
                            start.setDate(start.getDate() - phase1);
                        }
                        
                        // If within period, add.
                        if (start <= period && end >= period) add = true;
                    });
                    
                    if (add) periods[periods.length - 1].trajects.push(traject);
                }
            }
        });

        // Go back a month for the period and add a category for the chart
        period.setMonth(period.getMonth() - 1);
        categories.push(monthNames[period.getMonth()] + " " + period.getFullYear());
    }

    // Reverse the arrays since we looped back in time.
    categories = categories.reverse();
    periods = periods.reverse();
    return { categories: categories, xaxis: periods };
}

function filterPerPhase(result, settings) {
    
    // First get a collection of all phases in the result
    let phaseNames = new Set();
    result.data.forEach(entry => { if (entry.phase) phaseNames.add(getPhaseAlias(entry.phase.base_name)); });

    // Get all the categories and sort them according to the PhaseIndex
    let categories = Array.from(phaseNames);
    categories.sort((a, b) => PhaseIndex[a] - PhaseIndex[b]);

    let phases = [];
    for (let i = 0; i < categories.length; i++) {

        // Create entry for this phase
        // and find all entries that match this phase
        phases.push({ trajects: [] });
        result.data.forEach(entry => {
            if (entry.phase && getPhaseAlias(entry.phase.base_name) == categories[i])
                phases[phases.length - 1].trajects.push(entry);
        });
    }

    return { categories: categories, xaxis: phases };
}

// grouping filtering
// Groups the xaxis elements into groups, for use in stacked
// area or column graph where each 'x' has multiple values.
function groupByProcedure(xaxis, settings) {

    // Find all the unique procedures.
    // Populate the series with all the procedure types
    // Push all the trajects into all the groups they belong in.
    let procedures = new Set(), series = [], index = 0;
    xaxis.forEach(elem => elem.trajects.forEach(traject => procedures.add(traject.procedure.name)));
    procedures.forEach(proc => series.push({ type: settings.chart, name: proc, data: Array.from(Array(settings.count), () => []) }));
    xaxis.forEach(elem => {
        elem.trajects.forEach(traject => series.find(element => element.name == traject.procedure.name).data[index].push(traject)); 
        index++;
    });

    return series;
}

function groupByBuyer(xaxis, settings) {
    const unknownName = "Onbekend";
    
    // Find all the unique buyers.
    // Populate the series with all the buyers
    // Push all the trajects into all the groups they belong in.
    let buyers = new Set(), series = [], index = 0;
    xaxis.forEach(elem => elem.trajects.forEach(traject => buyers.add(traject.buyer ? traject.buyer.first_name + " " + traject.buyer.last_name : unknownName)));
    buyers.forEach(buyer => series.push({ type: settings.chart, name: buyer, data: Array.from(Array(settings.count), () => []) }));
    xaxis.forEach(elem => {
        elem.trajects.forEach(traject => {
            if (traject.buyer)
                series.find(element => element.name == 
                    traject.buyer.first_name + " " + traject.buyer.last_name).data[index].push(traject);
            else
                series.find(element => element.name == unknownName).data[index].push(traject);
            }); 
        index++;
    });

    return series;
}

function groupByPhase(xaxis, settings) {
    
    // Find all the unique phases.
    // Populate the series with all the phases
    // Push all the trajects into all the groups they belong in.
    let phases = new Set(), series = [], index = 0;
    xaxis.forEach(elem => elem.trajects.forEach(traject => {
        if (traject.phase)
            phases.add(getPhaseAlias(traject.phase.base_name));
    }));
    phases.forEach(phase => series.push({ type: settings.chart, name: phase, data: Array.from(Array(settings.count), () => []) }));
    xaxis.forEach(elem => {
        elem.trajects.forEach(traject => {
            if (traject.phase)
                series.find(element => element.name == getPhaseAlias(traject.phase.base_name)).data[index].push(traject);
        }); 
        index++;
    });

    series.sort((a, b) => PhaseIndex[a.name] - PhaseIndex[b.name]);

    return series;
}

// y-axis data
// the final step is displaying some number for each 'x' for each group
function getCount(series, settings) {
    let result = [];
    series.forEach(x => {
        result.push({ type: x.type, name: x.name, data: new Array(settings.count).fill(0) });
        let index = 0;
        x.data.forEach(group => {
            result[result.length - 1].data[index] = group.length; // Count amount of trajects in group
            index++;
        });
    });
    return result;
}

function getBuyerHours(series, settings) {
    let result = [];
    series.forEach(x => {
        result.push({ type: x.type, name: x.name, data: new Array(settings.count).fill(0) });
        let index = 0;
        x.data.forEach(group => {
            let sumhours = 0; // Sum up all the buyer hours in this group
            group.forEach(traject => { sumhours += traject.procedure.buyer_hours; });
            result[result.length - 1].data[index] = sumhours; 
            index++;
        });
    });
    return result;
}

function updateLineChart(result, chart, settings) {
    let categories = [], xaxis = [], series = [];

    // Determine data for the xaxis
    switch (settings.xaxis) {
    case ChartSettings.ForEachMonth:
        var res = filterPerMonth(result, settings, chart);
        categories = res.categories;
        xaxis = res.xaxis;
        settings.count = categories.length;
        break;
    case ChartSettings.ForEachPhase:
        var res = filterPerPhase(result, settings);
        categories = res.categories;
        xaxis = res.xaxis;
        settings.count = categories.length;
        break;
    }

    switch (settings.group) {
    case ChartSettings.PerProcedure: series = groupByProcedure(xaxis, settings); break;
    case ChartSettings.PerBuyer: series = groupByBuyer(xaxis, settings); break;
    case ChartSettings.PerPhase: series = groupByPhase(xaxis, settings); break;
    }

    switch (settings.yaxis) {
    case ChartSettings.ShowCount: series = getCount(series, settings); break;
    case ChartSettings.ShowBuyerHours: series = getBuyerHours(series, settings); break;
    }

    // Update the chart
    chart.chartOptions = { xaxis: { categories: categories }, };
    chart.series = series;
}

// Group filtering
// for the pie charts
function pieGroupProcedure(data) {

    // Find all the unique procedures using a Set.
    // Populate the series with all the procedure types
    // Push all the trajects into all the groups they belong in.
    let procedures = new Set(), series = [];
    data.forEach(traject => procedures.add(traject.procedure.name)); 
    procedures.forEach(proc => series.push({ name: proc, data: [] }));
    data.forEach(traject => series.find(element => element.name == traject.procedure.name).data.push(traject));
    return series;
}

// Value calculations
// for the pie charts
function getAverageLeadPeriod(data) {
    let result = [];
    data.forEach(elem => {
        let total = 0, amount = 0;
        elem.data.forEach(traject => {
            total += traject.procedure.lead_period;
            amount++;
        });

        let average = total / amount;
        result.push({ name: elem.name, data: average });
    });
    return result;
}

function getAverageBuyerHours(data) {
    let result = [];
    data.forEach(elem => {
        let total = 0, amount = 0;
        elem.data.forEach(traject => {
            total += traject.procedure.buyer_hours;
            amount++;
        });

        let average = total / amount;
        result.push({ name: elem.name, data: average });
    });
    return result;
}

function updatePieChart(result, chart, settings) {

    let data = [];
    switch (settings.group) {
    case ChartSettings.PerProcedure: data = pieGroupProcedure(result.data); break;
    }

    switch (settings.show) {
    case ChartSettings.ShowAverageLeadPeriod: data = getAverageLeadPeriod(data); break;
    case ChartSettings.ShowAverageBuyerHours: data = getAverageBuyerHours(data); break;
    }

    // Sort pie chart on data, and filter 0s.
    data.sort((a, b) => b.data - a.data);
    data = data.filter(a => a.data > 0);

    let labels = [], series = [];
    data.forEach(elem => {
        labels.push(elem.name);
        series.push(elem.data);
    });

    chart.chartOptions = { 
        labels: labels, 
        chart: { type: 'donut' },
        tooltip: { enabled: false },
        plotOptions: { pie: { donut: { labels: {
            show: true,
            value: { formatter: function(val) { return val + " " + settings.unit } }
        } } } },
        colors: Colors,
        dataLabels: { 
            enabled: true, 
            style: { fontSize: '15px' },
            dropShadow: { color: '#000', blur: 1, top: 0, left: 0, opacity: 1 },
            formatter: function(val, opt) { return opt.w.config.series[opt.seriesIndex]; }
        },
    };
    chart.series = series;
}

function setChartData(chart, data) {

    // Find all the unique buyers.
    let buyers = [];
    data.forEach(traject => {
        if (traject.buyer) {
            if (buyers.filter(elem => elem.id == traject.buyer.id).length == 0)
                buyers.push({ id: traject.buyer.id, name: traject.buyer.first_name + " " + traject.buyer.last_name });
        }
    }); 

    // Find all the unique phases.
    let realphases = [];
    data.forEach(traject => {
        if (traject.phase)
            if (realphases.filter(elem => elem.id == traject.phase.base_id).length == 0)
                realphases.push({ id: traject.phase.base_id, name: traject.phase.base_name });
    });
    let calculatedphases = [ // Calculated using dates
        { id: 0, name: "Voorbereiden" }, 
        { id: 1, name: "Publicatiefase" },
        { id: 2, name: "Beoordelingsfase" },
        { id: 3, name: "Bezwaartermijn" },
    ];
    
    // Find all the unique procedures.
    let procedures = [];
    data.forEach(traject => {
        if (traject.procedure)
            if (procedures.filter(elem => elem.id == traject.procedure.id).length == 0)
                procedures.push({ id: traject.procedure.id, name: traject.procedure.name });
    });

    chart.allrealphases = realphases;
    chart.allcalculatedphases = calculatedphases;
    chart.allbuyers = buyers;
    chart.allprocedures = procedures;
}

Vue.mixin({
    methods: {
        updateChart: async function(chart, settings, query = '') {
            let q = query;
            if (chart.onlyActive)
                q += 'state=active';

            await axios.get("/api/trajects?" + q).then(result => {
                if (chart.type == 'line' || chart.type == 'area')
                    updateLineChart(result, chart, settings);
                else if (chart.type == 'pie')
                    updatePieChart(result, chart, settings);

                // If no filter, set data in the graph
                if (query == "") 
                    setChartData(chart, result.data);
            });
        },

        // Update function for the displayed numbers at the top
        updateNumber: async function(nmb, settings) {
            let query = '';
            switch(settings.type) {
            case ChartSettings.NmrFinishedTrajects:  query = 'state=finished'; break;
            case ChartSettings.NmrLongtermTrajects:  query = 'longterm=true'; break;
            case ChartSettings.NmrRunningTrajects:   query = 'state=active'; break;
            case ChartSettings.NmrRiskTrajects:      query = 'risk=true'; break;
            case ChartSettings.NmrTrajectsToStart:   query = 'state=pre_active'; break;
            case ChartSettings.NmrOnHoldTrajects:    query = 'phases=8'; break;
            case ChartSettings.NmrWithdrawnTrajects: query = 'phases=9'; break;
            }

            await axios.get("/api/trajects?" + query).then(result => {
                nmb.trajects = result.data;
                nmb.number = result.data.length;
            });
        }
    }
});