require('./bootstrap');

window.Vue = require('vue').default;

import Apex from 'vue-apexcharts'
import Multiselect from 'vue-multiselect'
import BootstrapVue from 'bootstrap-vue'
import '../../node_modules/bootstrap-vue/dist/bootstrap-vue.css';
import axios from "axios";
Vue.use(Apex)
Vue.use(Multiselect)
Vue.use(BootstrapVue)

Vue.component('multiselect', Multiselect)
Vue.component('apexchart', Apex)
Vue.component('trajects-table', require('./components/TrajectsTable.vue').default)
Vue.component('column-chart', require('./components/ColumnChart.vue').default)
Vue.component('area-chart', require('./components/AreaChart.vue').default)
Vue.component('pie-chart', require('./components/PieChart.vue').default)
Vue.component('number', require('./components/Number.vue').default)

require('./updatecharts');

const app = new Vue({
    el: '#app',
    data : {
        ChartSettings,
        Colors
    },
});
