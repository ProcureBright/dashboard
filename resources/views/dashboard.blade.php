<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link href="css/vue-multiselect.min.css" rel="stylesheet" type="text/css"/>
		<link href="css/app.css" rel="stylesheet" type="text/css" />

        <link href="css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="css/ubold.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
        <link href="css/icons.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div id="wrapper">
            <div class="content-page">
                <div class="content">
                    <div id="app" class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dashboard</h4>
                                </div>
                            </div>
                        </div>     

                        <div class="row">
                            <number 
                                :type="ChartSettings.NmrRunningTrajects"
                                text="Lopende trajecten"></number>
    
                            <number 
                                :type="ChartSettings.NmrTrajectsToStart" 
                                text="Op te starten trajecten"></number>
                            
                            <number 
                                :type="ChartSettings.NmrFinishedTrajects"
                                text="Afgeronde trajecten"></number>
                            
                            <number 
                                :type="ChartSettings.NmrRiskTrajects"
                                text="Risico trajecten"></number>

                            <number 
                                :type="ChartSettings.NmrOnHoldTrajects"
                                text="On hold trajecten"></number>

                            <number 
                                :type="ChartSettings.NmrWithdrawnTrajects"
                                text="Teruggetrokken trajecten"></number>
                        </div>

                        <div class="row">
                                <div class="col-12">
                                    <area-chart></area-chart>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-12">
                                        <pie-chart 
                                            :content="ChartSettings.ShowAverageLeadPeriod" 
                                            unit="Maanden" 
                                            title="Gemiddelde Doorlooptijd Per Procedure (Maanden)"></pie-chart>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-12">
                                        <pie-chart 
                                            :content="ChartSettings.ShowAverageBuyerHours" 
                                            unit="Uur" 
                                            title="Gemiddelde Inzet Inkoper Per Procedure (Uur)"></pie-chart>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/app.js"></script>
        <script src="js/vendor.min.js"></script>
        <!-- <script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>
        <script src="https://apexcharts.com/samples/assets/ohlc.js"></script>
        <script src="js/apexcharts.init.js"></script>
        <script src="js/ubold.min.js"></script> -->
    </body>
</html>