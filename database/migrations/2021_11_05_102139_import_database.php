<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImportDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `traject_phases`;');
        DB::unprepared(file_get_contents("./procureplan_DB.sql"));

        // Create a view of the traject phases so we can access the base id 
        // of a certain phase as base_id, instead of having to deal with null values.
        DB::statement('CREATE VIEW `traject_phases` AS 
            select 
                t1.id, 
                t1.name, 
                t1.base_id, 
                t2.name as base_name 
            from 
                (select # Sub query to get id, name, and base_id of a status.
                    t.id, 
                    t.name,
                    (select # Sub query to get parent id, or id if parent id is null 
                        min(id) 
                    from 
                        traject_statuses 
                    where 
                        id = t.parent_id 
                     OR id = t.id) 
                as base_id 
                from 
                    traject_statuses t) 
                t1, 
                traject_statuses t2 
            where 
                t1.base_id = t2.id; # Final merge to also get the base_name
            '
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
