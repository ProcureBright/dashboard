/*
 Navicat MySQL Data Transfer

 Source Server         : AWS Production
 Source Server Type    : MySQL
 Source Server Version : 50712
 Source Host           : proplan.cluster-c658mrdcuj9l.eu-central-1.rds.amazonaws.com:3306
 Source Schema         : procureplan

 Target Server Type    : MySQL
 Target Server Version : 50712
 File Encoding         : 65001

 Date: 04/11/2021 14:42:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for 5fd14227b83e8
-- ----------------------------
DROP TABLE IF EXISTS `5fd14227b83e8`;
CREATE TABLE `5fd14227b83e8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of 5fd14227b83e8
-- ----------------------------
BEGIN;
INSERT INTO `5fd14227b83e8` VALUES (1, 'Afdeling 1');
INSERT INTO `5fd14227b83e8` VALUES (2, 'Afdeling 2');
COMMIT;

-- ----------------------------
-- Table structure for abilities
-- ----------------------------
DROP TABLE IF EXISTS `abilities`;
CREATE TABLE `abilities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of abilities
-- ----------------------------
BEGIN;
INSERT INTO `abilities` VALUES (1, 'read_only', 'Read Only', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `abilities` VALUES (2, 'create', 'Create', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `abilities` VALUES (3, 'read', 'Read', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `abilities` VALUES (4, 'edit', 'Edit', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `abilities` VALUES (5, 'remove', 'Remove', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `abilities` VALUES (6, 'admin', 'Internal Admin', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `abilities` VALUES (7, 'superAdmin', 'External Admin', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
COMMIT;

-- ----------------------------
-- Table structure for ability_role
-- ----------------------------
DROP TABLE IF EXISTS `ability_role`;
CREATE TABLE `ability_role` (
  `role_id` bigint(20) unsigned NOT NULL,
  `ability_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`,`ability_id`),
  KEY `ability_role_ability_id_foreign` (`ability_id`),
  CONSTRAINT `ability_role_ability_id_foreign` FOREIGN KEY (`ability_id`) REFERENCES `abilities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ability_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of ability_role
-- ----------------------------
BEGIN;
INSERT INTO `ability_role` VALUES (6, 6, '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `ability_role` VALUES (7, 7, '2020-11-10 08:50:57', '2020-11-10 08:50:57');
COMMIT;

-- ----------------------------
-- Table structure for active_headers
-- ----------------------------
DROP TABLE IF EXISTS `active_headers`;
CREATE TABLE `active_headers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1684 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of active_headers
-- ----------------------------
BEGIN;
INSERT INTO `active_headers` VALUES (356, 1, 18);
INSERT INTO `active_headers` VALUES (357, 2, 18);
INSERT INTO `active_headers` VALUES (358, 12, 18);
INSERT INTO `active_headers` VALUES (727, 1, 22);
INSERT INTO `active_headers` VALUES (728, 2, 22);
INSERT INTO `active_headers` VALUES (729, 4, 22);
INSERT INTO `active_headers` VALUES (730, 7, 22);
INSERT INTO `active_headers` VALUES (731, 10, 22);
INSERT INTO `active_headers` VALUES (732, 12, 22);
INSERT INTO `active_headers` VALUES (733, 20, 22);
INSERT INTO `active_headers` VALUES (734, 11, 22);
INSERT INTO `active_headers` VALUES (1221, 1, 1);
INSERT INTO `active_headers` VALUES (1222, 2, 1);
INSERT INTO `active_headers` VALUES (1223, 3, 1);
INSERT INTO `active_headers` VALUES (1224, 4, 1);
INSERT INTO `active_headers` VALUES (1225, 6, 1);
INSERT INTO `active_headers` VALUES (1226, 21, 1);
INSERT INTO `active_headers` VALUES (1227, 48, 1);
INSERT INTO `active_headers` VALUES (1228, 47, 1);
INSERT INTO `active_headers` VALUES (1549, 1, 21);
INSERT INTO `active_headers` VALUES (1550, 2, 21);
INSERT INTO `active_headers` VALUES (1551, 3, 21);
INSERT INTO `active_headers` VALUES (1552, 4, 21);
INSERT INTO `active_headers` VALUES (1553, 7, 21);
INSERT INTO `active_headers` VALUES (1554, 8, 21);
INSERT INTO `active_headers` VALUES (1555, 10, 21);
INSERT INTO `active_headers` VALUES (1556, 46, 21);
INSERT INTO `active_headers` VALUES (1557, 21, 21);
INSERT INTO `active_headers` VALUES (1666, 1, 2);
INSERT INTO `active_headers` VALUES (1667, 2, 2);
INSERT INTO `active_headers` VALUES (1668, 4, 2);
INSERT INTO `active_headers` VALUES (1669, 7, 2);
INSERT INTO `active_headers` VALUES (1670, 10, 2);
INSERT INTO `active_headers` VALUES (1671, 47, 2);
INSERT INTO `active_headers` VALUES (1672, 3, 2);
INSERT INTO `active_headers` VALUES (1673, 5, 2);
INSERT INTO `active_headers` VALUES (1674, 12, 2);
INSERT INTO `active_headers` VALUES (1675, 1, 27);
INSERT INTO `active_headers` VALUES (1676, 2, 27);
INSERT INTO `active_headers` VALUES (1677, 3, 27);
INSERT INTO `active_headers` VALUES (1678, 4, 27);
INSERT INTO `active_headers` VALUES (1679, 7, 27);
INSERT INTO `active_headers` VALUES (1680, 10, 27);
INSERT INTO `active_headers` VALUES (1681, 46, 27);
INSERT INTO `active_headers` VALUES (1682, 48, 27);
INSERT INTO `active_headers` VALUES (1683, 5, 27);
COMMIT;

-- ----------------------------
-- Table structure for active_search_headers
-- ----------------------------
DROP TABLE IF EXISTS `active_search_headers`;
CREATE TABLE `active_search_headers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=420 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of active_search_headers
-- ----------------------------
BEGIN;
INSERT INTO `active_search_headers` VALUES (186, 1, 1);
INSERT INTO `active_search_headers` VALUES (187, 2, 1);
INSERT INTO `active_search_headers` VALUES (188, 4, 1);
INSERT INTO `active_search_headers` VALUES (189, 7, 1);
INSERT INTO `active_search_headers` VALUES (190, 8, 1);
INSERT INTO `active_search_headers` VALUES (191, 9, 1);
INSERT INTO `active_search_headers` VALUES (192, 10, 1);
INSERT INTO `active_search_headers` VALUES (193, 20, 1);
INSERT INTO `active_search_headers` VALUES (194, 6, 1);
INSERT INTO `active_search_headers` VALUES (195, 3, 1);
INSERT INTO `active_search_headers` VALUES (196, 12, 1);
INSERT INTO `active_search_headers` VALUES (197, 11, 1);
INSERT INTO `active_search_headers` VALUES (271, 1, 18);
INSERT INTO `active_search_headers` VALUES (272, 4, 18);
INSERT INTO `active_search_headers` VALUES (273, 7, 18);
INSERT INTO `active_search_headers` VALUES (379, 1, 21);
INSERT INTO `active_search_headers` VALUES (380, 2, 21);
INSERT INTO `active_search_headers` VALUES (381, 9, 21);
INSERT INTO `active_search_headers` VALUES (382, 48, 21);
INSERT INTO `active_search_headers` VALUES (383, 7, 21);
INSERT INTO `active_search_headers` VALUES (384, 8, 21);
INSERT INTO `active_search_headers` VALUES (385, 4, 21);
INSERT INTO `active_search_headers` VALUES (386, 6, 21);
INSERT INTO `active_search_headers` VALUES (403, 1, 2);
INSERT INTO `active_search_headers` VALUES (404, 2, 2);
INSERT INTO `active_search_headers` VALUES (405, 4, 2);
INSERT INTO `active_search_headers` VALUES (406, 6, 2);
INSERT INTO `active_search_headers` VALUES (407, 7, 2);
INSERT INTO `active_search_headers` VALUES (408, 48, 2);
INSERT INTO `active_search_headers` VALUES (414, 2, 27);
INSERT INTO `active_search_headers` VALUES (415, 5, 27);
INSERT INTO `active_search_headers` VALUES (416, 6, 27);
INSERT INTO `active_search_headers` VALUES (417, 7, 27);
INSERT INTO `active_search_headers` VALUES (418, 48, 27);
INSERT INTO `active_search_headers` VALUES (419, 3, 27);
COMMIT;

-- ----------------------------
-- Table structure for application
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_checked` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `feedback` longtext COLLATE utf8mb4_0900_ai_ci,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `checked` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of application
-- ----------------------------
BEGIN;
INSERT INTO `application` VALUES (1, 2, 2, '2021-10-18 12:16:57', NULL, '2021-10-18 12:16:57', '2021-10-06 13:47:32', 2);
COMMIT;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for form_headings
-- ----------------------------
DROP TABLE IF EXISTS `form_headings`;
CREATE TABLE `form_headings` (
  `id` int(11) NOT NULL,
  `section` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of form_headings
-- ----------------------------
BEGIN;
INSERT INTO `form_headings` VALUES (1, 1, 'Algemeen');
INSERT INTO `form_headings` VALUES (2, 2, 'Gebruikers');
INSERT INTO `form_headings` VALUES (3, 3, 'Overeenkomst');
INSERT INTO `form_headings` VALUES (4, 4, 'Termijnen');
COMMIT;

-- ----------------------------
-- Table structure for form_models
-- ----------------------------
DROP TABLE IF EXISTS `form_models`;
CREATE TABLE `form_models` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visible` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of form_models
-- ----------------------------
BEGIN;
INSERT INTO `form_models` VALUES (1, 'id', 'Id', 'number', 1, 2, 'traject', NULL, NULL, '1', '1', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (2, 'name', 'Naam traject', 'text', 1, 1, 'traject', NULL, NULL, '1', '1', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (3, 'procedure_id', 'Procedure', 'select', 2, 1, 'procedure', NULL, NULL, '1', '1', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (4, 'time', 'Doorlooptijd procedure (maanden)', 'number', 3, 1, 'traject', NULL, NULL, '1', '1', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (5, 'status_id', 'Status', 'select', 4, 1, 'status', NULL, NULL, '1', '1', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (6, 'clm_id', 'Contractmanager', 'select', 1, 1, 'user', NULL, NULL, '1', '2', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (7, 'buyer_id', 'Inkoper', 'select', 1, 2, 'user', NULL, NULL, '1', '2', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (8, 'reader_id', 'Meelezer', 'select', 2, 1, 'user', NULL, NULL, '1', '2', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (9, 'legal_id', 'Jurist', 'select', 2, 2, 'user', NULL, NULL, '1', '2', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (10, 'start_contract', 'Verwachte ingangsdatum ovk', 'date', 1, 1, 'traject', NULL, NULL, '1', '3', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (11, 'end_contract', 'Einddatum huidige ovk', 'date', 1, 2, 'traject', NULL, NULL, '1', '3', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (12, 'publication', 'Datum publicatie', 'date', 1, 1, 'traject', NULL, NULL, '1', '4', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (13, 'nvi_one', 'Uiterste datum stellen vragen NvI 1', 'date', 2, 1, 'traject', NULL, NULL, '1', '4', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (14, 'nvi_two', 'Uiterste datum stellen vragen NvI 2', 'date', 3, 1, 'traject', NULL, NULL, '1', '4', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (15, 'max_subscribe', 'Datum uiterste inschrijving', 'date', 1, 2, 'traject', NULL, NULL, '1', '4', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (16, 'nomination', 'Datum gunningsbeslissing', 'date', 2, 2, 'traject', NULL, NULL, '1', '4', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (17, 'clm_cap', 'Inzet CLM', 'number', 1, 2, 'traject', NULL, NULL, '0', '5', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (18, 'legal_cap', 'Inzet Jurist', 'number', 2, 2, 'traject', NULL, NULL, '0', '5', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (19, 'reader_cap', 'Inzet Meelezer', 'number', 2, 1, 'traject', NULL, NULL, '0', '5', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (20, 'buyer_cap', 'Inzet inkoper', 'number', 1, 1, 'traject', NULL, NULL, '1', '5', '2020-11-10 08:50:58', '2020-11-10 08:50:58');
INSERT INTO `form_models` VALUES (21, 'emoij', 'Projectvoortgang', 'number', 0, 0, 'traject', NULL, NULL, '0', '0', '2021-01-27 09:40:06', '2021-01-27 09:40:06');
INSERT INTO `form_models` VALUES (46, 'traject_start', 'Startdatum traject (automatisch)', 'date', 3, 1, 'traject', NULL, NULL, '1', '4', '2021-02-10 15:33:11', '2021-02-10 15:33:11');
INSERT INTO `form_models` VALUES (47, 'custom_603cbe0a12b29', 'Opmerkingen', 'textarea', 1, 1, 'traject', NULL, NULL, '1', '6', NULL, NULL);
INSERT INTO `form_models` VALUES (48, 'custom_5fd14227b83f1', 'Afdeling', 'select', 2, 2, '5fd14227b83e8', NULL, NULL, '1', '1', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for login_histories
-- ----------------------------
DROP TABLE IF EXISTS `login_histories`;
CREATE TABLE `login_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of login_histories
-- ----------------------------
BEGIN;
INSERT INTO `login_histories` VALUES (1, 'Radek', 'rdolewa@procurance.nl', '2021-07-29', '2021-07-29');
INSERT INTO `login_histories` VALUES (2, 'Rens', 'rens@procurebright.nl', '2021-08-02', '2021-08-02');
INSERT INTO `login_histories` VALUES (3, 'Radek', 'rdolewa@procurance.nl', '2021-08-02', '2021-08-02');
INSERT INTO `login_histories` VALUES (4, 'Radek', 'rdolewa@procurance.nl', '2021-08-03', '2021-08-03');
INSERT INTO `login_histories` VALUES (5, 'Radek', 'rdolewa@procurance.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (6, 'Rens', 'rens@procurebright.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (7, 'Rens', 'rens@procurebright.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (8, 'Radek', 'rdolewa@procurance.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (9, 'Rens', 'rens@procurebright.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (10, 'Rens', 'rens@procurebright.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (11, 'Radek', 'rdolewa@procurance.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (12, 'Rens', 'rens@procurebright.nl', '2021-08-04', '2021-08-04');
INSERT INTO `login_histories` VALUES (13, 'Radek', 'rdolewa@procurance.nl', '2021-08-05', '2021-08-05');
INSERT INTO `login_histories` VALUES (14, 'Rens', 'rens@procurebright.nl', '2021-08-05', '2021-08-05');
INSERT INTO `login_histories` VALUES (15, 'Radek', 'rdolewa@procurance.nl', '2021-08-09', '2021-08-09');
INSERT INTO `login_histories` VALUES (16, 'Radek', 'rdolewa@procurance.nl', '2021-08-09', '2021-08-09');
INSERT INTO `login_histories` VALUES (17, 'Rens', 'rens@procurebright.nl', '2021-08-09', '2021-08-09');
INSERT INTO `login_histories` VALUES (18, 'Radek', 'rdolewa@procurance.nl', '2021-08-09', '2021-08-09');
INSERT INTO `login_histories` VALUES (19, 'Rens', 'rens@procurebright.nl', '2021-08-10', '2021-08-10');
INSERT INTO `login_histories` VALUES (20, 'Radek', 'rdolewa@procurance.nl', '2021-08-10', '2021-08-10');
INSERT INTO `login_histories` VALUES (21, 'Rens', 'rens@procurebright.nl', '2021-08-11', '2021-08-11');
INSERT INTO `login_histories` VALUES (22, 'Rens', 'rens@procurebright.nl', '2021-08-12', '2021-08-12');
INSERT INTO `login_histories` VALUES (23, 'Rens', 'rens@procurebright.nl', '2021-08-13', '2021-08-13');
INSERT INTO `login_histories` VALUES (24, 'Rens', 'rens@procurebright.nl', '2021-08-16', '2021-08-16');
INSERT INTO `login_histories` VALUES (25, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (26, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (27, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (28, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (29, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (30, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (31, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (32, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (33, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (34, 'Rens', 'rens@procurebright.nl', '2021-08-17', '2021-08-17');
INSERT INTO `login_histories` VALUES (35, 'Rens', 'rens@procurebright.nl', '2021-08-18', '2021-08-18');
INSERT INTO `login_histories` VALUES (36, 'Radek', 'rdolewa@procurance.nl', '2021-08-18', '2021-08-18');
INSERT INTO `login_histories` VALUES (37, 'Rens', 'rens@procurebright.nl', '2021-08-18', '2021-08-18');
INSERT INTO `login_histories` VALUES (38, 'Rens', 'rens@procurebright.nl', '2021-08-18', '2021-08-18');
INSERT INTO `login_histories` VALUES (39, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-08-18', '2021-08-18');
INSERT INTO `login_histories` VALUES (40, 'Rens', 'rens@procurebright.nl', '2021-08-19', '2021-08-19');
INSERT INTO `login_histories` VALUES (41, 'Koen', 'kfranken@fsb-ssc.nl', '2021-08-19', '2021-08-19');
INSERT INTO `login_histories` VALUES (42, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-08-19', '2021-08-19');
INSERT INTO `login_histories` VALUES (43, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-08-19', '2021-08-19');
INSERT INTO `login_histories` VALUES (44, 'Rens', 'rens@procurebright.nl', '2021-08-19', '2021-08-19');
INSERT INTO `login_histories` VALUES (45, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-08-19', '2021-08-19');
INSERT INTO `login_histories` VALUES (46, 'Koen', 'kfranken@fsb-ssc.nl', '2021-08-20', '2021-08-20');
INSERT INTO `login_histories` VALUES (47, 'Rens', 'rens@procurebright.nl', '2021-08-23', '2021-08-23');
INSERT INTO `login_histories` VALUES (48, 'Rens', 'rens@procurebright.nl', '2021-08-23', '2021-08-23');
INSERT INTO `login_histories` VALUES (49, 'Radek', 'rdolewa@procurance.nl', '2021-08-23', '2021-08-23');
INSERT INTO `login_histories` VALUES (50, 'Rens', 'rens@procurebright.nl', '2021-08-24', '2021-08-24');
INSERT INTO `login_histories` VALUES (51, 'Rens', 'rens@procurebright.nl', '2021-08-24', '2021-08-24');
INSERT INTO `login_histories` VALUES (52, 'Rens', 'rens@procurebright.nl', '2021-08-24', '2021-08-24');
INSERT INTO `login_histories` VALUES (53, 'Radek', 'rdolewa@procurance.nl', '2021-08-24', '2021-08-24');
INSERT INTO `login_histories` VALUES (54, 'Rens', 'rens@procurebright.nl', '2021-08-25', '2021-08-25');
INSERT INTO `login_histories` VALUES (55, 'Rens', 'rens@procurebright.nl', '2021-08-25', '2021-08-25');
INSERT INTO `login_histories` VALUES (56, 'Radek', 'test-4f8pyrm3c@srv1.mail-tester.com', '2021-08-25', '2021-08-25');
INSERT INTO `login_histories` VALUES (57, 'Rens', 'rens@procurebright.nl', '2021-08-26', '2021-08-26');
INSERT INTO `login_histories` VALUES (58, 'Rens', 'rens@procurebright.nl', '2021-08-26', '2021-08-26');
INSERT INTO `login_histories` VALUES (59, 'Rens', 'rens@procurebright.nl', '2021-08-27', '2021-08-27');
INSERT INTO `login_histories` VALUES (60, 'Rens', 'rens@procurebright.nl', '2021-08-30', '2021-08-30');
INSERT INTO `login_histories` VALUES (61, 'Rens', 'rens@procurebright.nl', '2021-08-30', '2021-08-30');
INSERT INTO `login_histories` VALUES (62, 'Rens', 'rens@procurebright.nl', '2021-08-31', '2021-08-31');
INSERT INTO `login_histories` VALUES (63, 'Rens', 'rens@procurebright.nl', '2021-08-31', '2021-08-31');
INSERT INTO `login_histories` VALUES (64, 'Rens', 'rens@procurebright.nl', '2021-08-31', '2021-08-31');
INSERT INTO `login_histories` VALUES (65, 'Rens', 'rens@procurebright.nl', '2021-09-01', '2021-09-01');
INSERT INTO `login_histories` VALUES (66, 'Rens', 'rens@procurebright.nl', '2021-09-01', '2021-09-01');
INSERT INTO `login_histories` VALUES (67, 'Rens', 'rens@procurebright.nl', '2021-09-02', '2021-09-02');
INSERT INTO `login_histories` VALUES (68, 'Rens', 'rens@procurebright.nl', '2021-09-06', '2021-09-06');
INSERT INTO `login_histories` VALUES (69, 'Rens', 'rens@procurebright.nl', '2021-09-06', '2021-09-06');
INSERT INTO `login_histories` VALUES (70, 'Rens', 'rens@procurebright.nl', '2021-09-07', '2021-09-07');
INSERT INTO `login_histories` VALUES (71, 'Rens', 'rens@procurebright.nl', '2021-09-08', '2021-09-08');
INSERT INTO `login_histories` VALUES (72, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-08', '2021-09-08');
INSERT INTO `login_histories` VALUES (73, 'Rens', 'rens@procurebright.nl', '2021-09-09', '2021-09-09');
INSERT INTO `login_histories` VALUES (74, 'Rens', 'rens@procurebright.nl', '2021-09-13', '2021-09-13');
INSERT INTO `login_histories` VALUES (75, 'Rens', 'rens@procurebright.nl', '2021-09-13', '2021-09-13');
INSERT INTO `login_histories` VALUES (76, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-13', '2021-09-13');
INSERT INTO `login_histories` VALUES (77, 'Rens', 'rens@procurebright.nl', '2021-09-14', '2021-09-14');
INSERT INTO `login_histories` VALUES (78, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-14', '2021-09-14');
INSERT INTO `login_histories` VALUES (79, 'Rens', 'rens@procurebright.nl', '2021-09-14', '2021-09-14');
INSERT INTO `login_histories` VALUES (80, 'Boudewijn', 'boudewijnvanimmerseel@gmail.com', '2021-09-14', '2021-09-14');
INSERT INTO `login_histories` VALUES (81, 'Rens', 'rens@procurebright.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (82, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (83, 'Rens', 'rens@procurebright.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (84, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (85, 'Radek', 'rdolewa@procurance.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (86, 'Rens', 'rens@procurebright.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (87, 'Rens', 'rens@procurebright.nl', '2021-09-15', '2021-09-15');
INSERT INTO `login_histories` VALUES (88, 'Rens', 'rens@procurebright.nl', '2021-09-16', '2021-09-16');
INSERT INTO `login_histories` VALUES (89, 'Radek', 'rdolewa@procurance.nl', '2021-09-16', '2021-09-16');
INSERT INTO `login_histories` VALUES (90, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-16', '2021-09-16');
INSERT INTO `login_histories` VALUES (91, 'Rens', 'rens@procurebright.nl', '2021-09-16', '2021-09-16');
INSERT INTO `login_histories` VALUES (92, 'Rens', 'rens@procurebright.nl', '2021-09-16', '2021-09-16');
INSERT INTO `login_histories` VALUES (93, 'Radek', 'rdolewa@procurance.nl', '2021-09-16', '2021-09-16');
INSERT INTO `login_histories` VALUES (94, 'Radek', 'rdolewa@procurance.nl', '2021-09-20', '2021-09-20');
INSERT INTO `login_histories` VALUES (95, 'Radek', 'rdolewa@procurance.nl', '2021-09-20', '2021-09-20');
INSERT INTO `login_histories` VALUES (96, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-09-20', '2021-09-20');
INSERT INTO `login_histories` VALUES (97, 'Rens', 'rens@procurebright.nl', '2021-09-21', '2021-09-21');
INSERT INTO `login_histories` VALUES (98, 'Rens', 'rens@procurebright.nl', '2021-09-22', '2021-09-22');
INSERT INTO `login_histories` VALUES (99, 'Radek', 'rdolewa@procurance.nl', '2021-09-22', '2021-09-22');
INSERT INTO `login_histories` VALUES (100, 'Rens', 'rens@procurebright.nl', '2021-09-24', '2021-09-24');
INSERT INTO `login_histories` VALUES (101, 'Rens', 'rens@procurebright.nl', '2021-09-24', '2021-09-24');
INSERT INTO `login_histories` VALUES (102, 'Rens', 'rens@procurebright.nl', '2021-09-24', '2021-09-24');
INSERT INTO `login_histories` VALUES (103, 'Rens', 'rens@procurebright.nl', '2021-09-27', '2021-09-27');
INSERT INTO `login_histories` VALUES (104, 'Rens', 'rens@procurebright.nl', '2021-09-27', '2021-09-27');
INSERT INTO `login_histories` VALUES (105, 'Rens', 'rens@procurebright.nl', '2021-09-27', '2021-09-27');
INSERT INTO `login_histories` VALUES (106, 'Rens', 'rens@procurebright.nl', '2021-09-29', '2021-09-29');
INSERT INTO `login_histories` VALUES (107, 'Rens', 'rens@procurebright.nl', '2021-09-29', '2021-09-29');
INSERT INTO `login_histories` VALUES (108, 'Rens', 'rens@procurebright.nl', '2021-09-30', '2021-09-30');
INSERT INTO `login_histories` VALUES (109, 'Rens', 'rens@procurebright.nl', '2021-10-01', '2021-10-01');
INSERT INTO `login_histories` VALUES (110, 'Rens', 'rens@procurebright.nl', '2021-10-01', '2021-10-01');
INSERT INTO `login_histories` VALUES (111, 'Rens', 'rens@procurebright.nl', '2021-10-01', '2021-10-01');
INSERT INTO `login_histories` VALUES (112, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-10-04', '2021-10-04');
INSERT INTO `login_histories` VALUES (113, 'Rens', 'rens@procurebright.nl', '2021-10-04', '2021-10-04');
INSERT INTO `login_histories` VALUES (114, 'Jasper', 'jasper@procurebright.nl', '2021-10-04', '2021-10-04');
INSERT INTO `login_histories` VALUES (115, 'Rens', 'rens@procurebright.nl', '2021-10-05', '2021-10-05');
INSERT INTO `login_histories` VALUES (116, 'Rens', 'rens@procurebright.nl', '2021-10-05', '2021-10-05');
INSERT INTO `login_histories` VALUES (117, 'Jasper', 'jasper@procurebright.nl', '2021-10-05', '2021-10-05');
INSERT INTO `login_histories` VALUES (118, 'Rens', 'rens@procurebright.nl', '2021-10-05', '2021-10-05');
INSERT INTO `login_histories` VALUES (119, 'Jasper', 'jasper@procurebright.nl', '2021-10-06', '2021-10-06');
INSERT INTO `login_histories` VALUES (120, 'Rens', 'rens@procurebright.nl', '2021-10-06', '2021-10-06');
INSERT INTO `login_histories` VALUES (121, 'Rens', 'rens@procurebright.nl', '2021-10-06', '2021-10-06');
INSERT INTO `login_histories` VALUES (122, 'Rens', 'rens@procurebright.nl', '2021-10-07', '2021-10-07');
INSERT INTO `login_histories` VALUES (123, 'Rens', 'rens@procurebright.nl', '2021-10-07', '2021-10-07');
INSERT INTO `login_histories` VALUES (124, 'Jasper', 'jasper@procurebright.nl', '2021-10-07', '2021-10-07');
INSERT INTO `login_histories` VALUES (125, 'Jasper', 'jasper@procurebright.nl', '2021-10-07', '2021-10-07');
INSERT INTO `login_histories` VALUES (126, 'Rens', 'rens@procurebright.nl', '2021-10-07', '2021-10-07');
INSERT INTO `login_histories` VALUES (127, 'Rens', 'rens@procurebright.nl', '2021-10-08', '2021-10-08');
INSERT INTO `login_histories` VALUES (128, 'Rens', 'rens@procurebright.nl', '2021-10-08', '2021-10-08');
INSERT INTO `login_histories` VALUES (129, 'Jasper', 'jasper@procurebright.nl', '2021-10-11', '2021-10-11');
INSERT INTO `login_histories` VALUES (130, 'Rens', 'rens@procurebright.nl', '2021-10-11', '2021-10-11');
INSERT INTO `login_histories` VALUES (131, 'Rens', 'rens@procurebright.nl', '2021-10-11', '2021-10-11');
INSERT INTO `login_histories` VALUES (132, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-10-11', '2021-10-11');
INSERT INTO `login_histories` VALUES (133, 'Jasper', 'jasper@procurebright.nl', '2021-10-12', '2021-10-12');
INSERT INTO `login_histories` VALUES (134, 'Rens', 'rens@procurebright.nl', '2021-10-12', '2021-10-12');
INSERT INTO `login_histories` VALUES (135, 'Jasper', 'jasper@procurebright.nl', '2021-10-12', '2021-10-12');
INSERT INTO `login_histories` VALUES (136, 'Rens', 'rens@procurebright.nl', '2021-10-12', '2021-10-12');
INSERT INTO `login_histories` VALUES (137, 'Jasper', 'jasper@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (138, 'Rens', 'rens@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (139, 'Jasper', 'jasper@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (140, 'Jasper', 'jasper@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (141, 'Jasper', 'jasper@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (142, 'Jasper', 'jasper@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (143, 'Jasper', 'jasper@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (144, 'Rens', 'rens@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (145, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-10-13', '2021-10-13');
INSERT INTO `login_histories` VALUES (146, 'Rens', 'rens@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (147, 'Jasper', 'jasper@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (148, 'Jasper', 'jasper@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (149, 'Rens', 'rens@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (150, 'Jasper', 'jasper@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (151, 'Rens', 'rens@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (152, 'Jasper', 'jasper@procurebright.nl', '2021-10-14', '2021-10-14');
INSERT INTO `login_histories` VALUES (153, 'Jasper', 'jasper@procurebright.nl', '2021-10-15', '2021-10-15');
INSERT INTO `login_histories` VALUES (154, 'Radek', 'rdolewa@procurance.nl', '2021-10-18', '2021-10-18');
INSERT INTO `login_histories` VALUES (155, 'Jasper', 'jasper@procurebright.nl', '2021-10-18', '2021-10-18');
INSERT INTO `login_histories` VALUES (156, 'Rens', 'rens@procurebright.nl', '2021-10-18', '2021-10-18');
INSERT INTO `login_histories` VALUES (157, 'Jasper', 'jasper@procurebright.nl', '2021-10-18', '2021-10-18');
INSERT INTO `login_histories` VALUES (158, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-10-18', '2021-10-18');
INSERT INTO `login_histories` VALUES (159, 'Jasper', 'jasper@procurebright.nl', '2021-10-19', '2021-10-19');
INSERT INTO `login_histories` VALUES (160, 'Radek', 'rdolewa@procurance.nl', '2021-10-19', '2021-10-19');
INSERT INTO `login_histories` VALUES (161, 'Rens', 'rens@procurebright.nl', '2021-10-19', '2021-10-19');
INSERT INTO `login_histories` VALUES (162, 'Rens', 'rens@procurebright.nl', '2021-10-19', '2021-10-19');
INSERT INTO `login_histories` VALUES (163, 'Jasper', 'jasper@procurebright.nl', '2021-10-20', '2021-10-20');
INSERT INTO `login_histories` VALUES (164, 'Jasper', 'jasper@procurebright.nl', '2021-10-20', '2021-10-20');
INSERT INTO `login_histories` VALUES (165, 'Radek', 'rdolewa@procurance.nl', '2021-10-21', '2021-10-21');
INSERT INTO `login_histories` VALUES (166, 'Jasper', 'jasper@procurebright.nl', '2021-10-21', '2021-10-21');
INSERT INTO `login_histories` VALUES (167, 'Jasper', 'jasper@procurebright.nl', '2021-10-22', '2021-10-22');
INSERT INTO `login_histories` VALUES (168, 'Jasper', 'jasper@procurebright.nl', '2021-10-25', '2021-10-25');
INSERT INTO `login_histories` VALUES (169, 'Jasper', 'jasper@procurebright.nl', '2021-10-26', '2021-10-26');
INSERT INTO `login_histories` VALUES (170, 'Radek', 'rdolewa@procurance.nl', '2021-10-26', '2021-10-26');
INSERT INTO `login_histories` VALUES (171, 'Jasper', 'jasper@procurebright.nl', '2021-10-27', '2021-10-27');
INSERT INTO `login_histories` VALUES (172, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-10-27', '2021-10-27');
INSERT INTO `login_histories` VALUES (173, 'Jasper', 'jasper@procurebright.nl', '2021-10-27', '2021-10-27');
INSERT INTO `login_histories` VALUES (174, 'Jasper', 'jasper@procurebright.nl', '2021-10-28', '2021-10-28');
INSERT INTO `login_histories` VALUES (175, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-10-28', '2021-10-28');
INSERT INTO `login_histories` VALUES (176, 'Jasper', 'jasper@procurebright.nl', '2021-10-28', '2021-10-28');
INSERT INTO `login_histories` VALUES (177, 'Jasper', 'jasper@procurebright.nl', '2021-10-29', '2021-10-29');
INSERT INTO `login_histories` VALUES (178, 'Jasper', 'jasper@procurebright.nl', '2021-11-01', '2021-11-01');
INSERT INTO `login_histories` VALUES (179, 'Jasper', 'jasper@procurebright.nl', '2021-11-01', '2021-11-01');
INSERT INTO `login_histories` VALUES (180, 'Jasper', 'jasper@procurebright.nl', '2021-11-02', '2021-11-02');
INSERT INTO `login_histories` VALUES (181, 'Jasper', 'jasper@procurebright.nl', '2021-11-03', '2021-11-03');
INSERT INTO `login_histories` VALUES (182, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-11-03', '2021-11-03');
INSERT INTO `login_histories` VALUES (183, 'Boudewijn', 'boudewijn@procurebright.nl', '2021-11-03', '2021-11-03');
INSERT INTO `login_histories` VALUES (184, 'Jasper', 'jasper@procurebright.nl', '2021-11-03', '2021-11-03');
INSERT INTO `login_histories` VALUES (185, 'Jasper', 'jasper@procurebright.nl', '2021-11-03', '2021-11-03');
INSERT INTO `login_histories` VALUES (186, 'Jasper', 'jasper@procurebright.nl', '2021-11-03', '2021-11-03');
INSERT INTO `login_histories` VALUES (187, 'Jasper', 'jasper@procurebright.nl', '2021-11-04', '2021-11-04');
INSERT INTO `login_histories` VALUES (188, 'Rens', 'rens@procurebright.nl', '2021-11-04', '2021-11-04');
INSERT INTO `login_histories` VALUES (189, 'Radek', 'rdolewa@procurance.nl', '2021-11-04', '2021-11-04');
INSERT INTO `login_histories` VALUES (190, 'Jasper', 'jasper@procurebright.nl', '2021-11-04', '2021-11-04');
INSERT INTO `login_histories` VALUES (191, 'Rens', 'rens@procurebright.nl', '2021-11-04', '2021-11-04');
INSERT INTO `login_histories` VALUES (192, 'Jasper', 'jasper@procurebright.nl', '2021-11-04', '2021-11-04');
COMMIT;

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `traject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=551 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of logs
-- ----------------------------
BEGIN;
INSERT INTO `logs` VALUES (1, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-06-07 06:58:48', 22);
INSERT INTO `logs` VALUES (2, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-07 06:58:48', 22);
INSERT INTO `logs` VALUES (3, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (4, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (5, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (6, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (7, 21, 'Rens Nibbeling heeft het veld Contractmanager gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (8, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (9, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (10, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (11, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (12, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-06-07 07:02:28', NULL);
INSERT INTO `logs` VALUES (13, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-06-07 07:07:11', 28);
INSERT INTO `logs` VALUES (14, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-07 07:07:11', 28);
INSERT INTO `logs` VALUES (15, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-06-07 07:07:11', 28);
INSERT INTO `logs` VALUES (16, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-07 07:07:11', 28);
INSERT INTO `logs` VALUES (17, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-07 07:07:11', 28);
INSERT INTO `logs` VALUES (18, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-06-07 07:07:11', 28);
INSERT INTO `logs` VALUES (19, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (20, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (21, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (22, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (23, 21, 'Rens Nibbeling heeft het veld Contractmanager gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (24, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (25, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (26, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (27, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (28, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-06-07 07:12:55', NULL);
INSERT INTO `logs` VALUES (29, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-07 07:15:09', 29);
INSERT INTO `logs` VALUES (30, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-07 07:15:09', 29);
INSERT INTO `logs` VALUES (31, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-06-07 07:15:43', 29);
INSERT INTO `logs` VALUES (32, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:16:12', 29);
INSERT INTO `logs` VALUES (33, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-07 07:16:43', 29);
INSERT INTO `logs` VALUES (34, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-07 07:16:43', 29);
INSERT INTO `logs` VALUES (35, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-07 07:16:43', 29);
INSERT INTO `logs` VALUES (36, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-07 07:16:58', 29);
INSERT INTO `logs` VALUES (37, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:18:48', 2);
INSERT INTO `logs` VALUES (38, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:20:10', 2);
INSERT INTO `logs` VALUES (39, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-07 07:23:05', 29);
INSERT INTO `logs` VALUES (40, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:26:44', 2);
INSERT INTO `logs` VALUES (41, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-07 07:28:32', 2);
INSERT INTO `logs` VALUES (42, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-08 08:22:38', 2);
INSERT INTO `logs` VALUES (43, 21, 'Rens Nibbeling heeft het veld Contractmanager gewijzigd', '2021-06-08 08:22:38', 2);
INSERT INTO `logs` VALUES (44, 21, 'Rens Nibbeling heeft het veld Meelezer gewijzigd', '2021-06-08 08:22:38', 2);
INSERT INTO `logs` VALUES (45, 21, 'Rens Nibbeling heeft het veld Jurist gewijzigd', '2021-06-08 08:22:38', 2);
INSERT INTO `logs` VALUES (46, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-08 08:22:38', 2);
INSERT INTO `logs` VALUES (47, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-08 08:24:12', 2);
INSERT INTO `logs` VALUES (48, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-06-08 08:26:47', 2);
INSERT INTO `logs` VALUES (49, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-08 08:26:47', 2);
INSERT INTO `logs` VALUES (50, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-08 08:26:47', 2);
INSERT INTO `logs` VALUES (51, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-06-08 11:17:29', 28);
INSERT INTO `logs` VALUES (52, 2, 'Boudewijn van Immerseel heeft het veld Status gewijzigd', '2021-06-08 13:17:27', 2);
INSERT INTO `logs` VALUES (53, 24, 'Annette Vos heeft het veld Inkoper gewijzigd', '2021-06-08 14:40:52', 2);
INSERT INTO `logs` VALUES (54, 24, 'Annette Vos heeft het veld Inkoper gewijzigd', '2021-06-08 14:45:36', 2);
INSERT INTO `logs` VALUES (55, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (56, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (57, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (58, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (59, 21, 'Rens Nibbeling heeft het veld Contractmanager gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (60, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (61, 21, 'Rens Nibbeling heeft het veld Meelezer gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (62, 21, 'Rens Nibbeling heeft het veld Jurist gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (63, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (64, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (65, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (66, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (67, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (68, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (69, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-06-09 12:39:49', NULL);
INSERT INTO `logs` VALUES (70, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-09 12:41:55', 30);
INSERT INTO `logs` VALUES (71, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-09 12:41:55', 30);
INSERT INTO `logs` VALUES (72, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-14 09:24:04', 22);
INSERT INTO `logs` VALUES (73, 21, 'Rens Nibbeling heeft het veld Meelezer gewijzigd', '2021-06-14 09:24:04', 22);
INSERT INTO `logs` VALUES (74, 22, 'Erik Dunnewind heeft het veld Naam traject gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (75, 22, 'Erik Dunnewind heeft het veld Procedure gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (76, 22, 'Erik Dunnewind heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (77, 22, 'Erik Dunnewind heeft het veld Status gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (78, 22, 'Erik Dunnewind heeft het veld Inkoper gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (79, 22, 'Erik Dunnewind heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (80, 22, 'Erik Dunnewind heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (81, 22, 'Erik Dunnewind heeft het veld Datum publicatie gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (82, 22, 'Erik Dunnewind heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (83, 22, 'Erik Dunnewind heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (84, 22, 'Erik Dunnewind heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (85, 22, 'Erik Dunnewind heeft het veld Inzet inkoper gewijzigd', '2021-06-15 14:02:26', NULL);
INSERT INTO `logs` VALUES (86, 22, 'Erik Dunnewind heeft het veld Datum publicatie gewijzigd', '2021-06-15 14:04:20', 31);
INSERT INTO `logs` VALUES (87, 22, 'Erik Dunnewind heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-15 14:04:20', 31);
INSERT INTO `logs` VALUES (88, 22, 'Erik Dunnewind heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-15 14:04:20', 31);
INSERT INTO `logs` VALUES (89, 22, 'Erik Dunnewind heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-15 14:04:20', 31);
INSERT INTO `logs` VALUES (90, 22, 'Erik Dunnewind heeft het veld Naam traject gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (91, 22, 'Erik Dunnewind heeft het veld Procedure gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (92, 22, 'Erik Dunnewind heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (93, 22, 'Erik Dunnewind heeft het veld Status gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (94, 22, 'Erik Dunnewind heeft het veld Contractmanager gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (95, 22, 'Erik Dunnewind heeft het veld Inkoper gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (96, 22, 'Erik Dunnewind heeft het veld Meelezer gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (97, 22, 'Erik Dunnewind heeft het veld Jurist gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (98, 22, 'Erik Dunnewind heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (99, 22, 'Erik Dunnewind heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (100, 22, 'Erik Dunnewind heeft het veld Datum publicatie gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (101, 22, 'Erik Dunnewind heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (102, 22, 'Erik Dunnewind heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (103, 22, 'Erik Dunnewind heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (104, 22, 'Erik Dunnewind heeft het veld Inzet inkoper gewijzigd', '2021-06-15 14:11:42', NULL);
INSERT INTO `logs` VALUES (105, 22, 'Erik Dunnewind heeft het veld Naam traject gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (106, 22, 'Erik Dunnewind heeft het veld Procedure gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (107, 22, 'Erik Dunnewind heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (108, 22, 'Erik Dunnewind heeft het veld Status gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (109, 22, 'Erik Dunnewind heeft het veld Contractmanager gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (110, 22, 'Erik Dunnewind heeft het veld Inkoper gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (111, 22, 'Erik Dunnewind heeft het veld Meelezer gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (112, 22, 'Erik Dunnewind heeft het veld Jurist gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (113, 22, 'Erik Dunnewind heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (114, 22, 'Erik Dunnewind heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (115, 22, 'Erik Dunnewind heeft het veld Datum publicatie gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (116, 22, 'Erik Dunnewind heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (117, 22, 'Erik Dunnewind heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (118, 22, 'Erik Dunnewind heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (119, 22, 'Erik Dunnewind heeft het veld Inzet inkoper gewijzigd', '2021-06-15 14:16:53', NULL);
INSERT INTO `logs` VALUES (120, 22, 'Erik Dunnewind heeft het veld Status gewijzigd', '2021-06-15 14:18:00', 32);
INSERT INTO `logs` VALUES (121, 21, 'Rens Nibbeling heeft het veld Jurist gewijzigd', '2021-06-16 11:55:24', 3);
INSERT INTO `logs` VALUES (122, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-16 13:53:07', 27);
INSERT INTO `logs` VALUES (123, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (124, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (125, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (126, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (127, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (128, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (129, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-06-16 14:44:41', NULL);
INSERT INTO `logs` VALUES (130, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-06-17 07:50:35', 26);
INSERT INTO `logs` VALUES (131, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-06-17 07:50:35', 26);
INSERT INTO `logs` VALUES (132, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-06-17 07:50:35', 26);
INSERT INTO `logs` VALUES (133, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-06-17 07:50:35', 26);
INSERT INTO `logs` VALUES (134, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-06-17 07:50:35', 26);
INSERT INTO `logs` VALUES (135, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-17 12:56:47', 2);
INSERT INTO `logs` VALUES (136, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-17 12:57:06', 2);
INSERT INTO `logs` VALUES (137, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-17 13:15:53', 21);
INSERT INTO `logs` VALUES (138, 24, 'Annette Vos heeft het veld Naam traject gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (139, 24, 'Annette Vos heeft het veld Procedure gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (140, 24, 'Annette Vos heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (141, 24, 'Annette Vos heeft het veld Status gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (142, 24, 'Annette Vos heeft het veld Contractmanager gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (143, 24, 'Annette Vos heeft het veld Inkoper gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (144, 24, 'Annette Vos heeft het veld Meelezer gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (145, 24, 'Annette Vos heeft het veld Jurist gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (146, 24, 'Annette Vos heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (147, 24, 'Annette Vos heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (148, 24, 'Annette Vos heeft het veld Inzet inkoper gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (149, 24, 'Annette Vos heeft het veld Afdeling gewijzigd', '2021-06-18 07:38:07', NULL);
INSERT INTO `logs` VALUES (150, 24, 'Annette Vos heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-18 07:39:17', 35);
INSERT INTO `logs` VALUES (151, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-18 13:39:17', 4);
INSERT INTO `logs` VALUES (152, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-18 13:39:17', 4);
INSERT INTO `logs` VALUES (153, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-06-18 13:39:17', 4);
INSERT INTO `logs` VALUES (154, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-06-18 14:29:43', 23);
INSERT INTO `logs` VALUES (155, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (156, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (157, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (158, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (159, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (160, 21, 'Rens Nibbeling heeft het veld Meelezer gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (161, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (162, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (163, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-06-21 12:14:00', NULL);
INSERT INTO `logs` VALUES (164, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-06-21 12:15:33', 36);
INSERT INTO `logs` VALUES (165, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-22 07:46:59', 34);
INSERT INTO `logs` VALUES (166, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-22 07:48:00', 34);
INSERT INTO `logs` VALUES (167, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-22 07:48:24', 34);
INSERT INTO `logs` VALUES (168, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-06-22 07:49:52', 34);
INSERT INTO `logs` VALUES (169, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-30 07:24:39', 6);
INSERT INTO `logs` VALUES (170, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-06-30 07:24:39', 6);
INSERT INTO `logs` VALUES (171, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-06-30 07:24:39', 6);
INSERT INTO `logs` VALUES (172, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-06-30 12:09:28', 7);
INSERT INTO `logs` VALUES (173, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-07-01 11:39:08', 7);
INSERT INTO `logs` VALUES (174, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-07-01 11:47:22', 7);
INSERT INTO `logs` VALUES (175, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (176, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (177, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (178, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (179, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (180, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (181, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-08-03 14:17:32', NULL);
INSERT INTO `logs` VALUES (182, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-13 11:25:23', 30);
INSERT INTO `logs` VALUES (183, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-13 11:27:46', 30);
INSERT INTO `logs` VALUES (184, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-13 11:28:04', 30);
INSERT INTO `logs` VALUES (185, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-08-13 11:33:10', 30);
INSERT INTO `logs` VALUES (186, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-08-13 11:33:10', 30);
INSERT INTO `logs` VALUES (187, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-08-13 11:41:28', 30);
INSERT INTO `logs` VALUES (188, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-08-13 11:41:28', 30);
INSERT INTO `logs` VALUES (189, 2, 'Boudewijn van Immerseel heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-17 08:34:35', 3);
INSERT INTO `logs` VALUES (190, 2, 'Boudewijn van Immerseel heeft het veld Procedure gewijzigd', '2021-08-17 08:44:03', 11);
INSERT INTO `logs` VALUES (191, 2, 'Boudewijn van Immerseel heeft het veld Status gewijzigd', '2021-08-17 08:44:03', 11);
INSERT INTO `logs` VALUES (192, 2, 'Boudewijn van Immerseel heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-17 08:44:47', 11);
INSERT INTO `logs` VALUES (193, 2, 'Boudewijn van Immerseel heeft het veld Status gewijzigd', '2021-08-17 08:45:10', 11);
INSERT INTO `logs` VALUES (194, 2, 'Boudewijn van Immerseel heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-17 08:45:10', 11);
INSERT INTO `logs` VALUES (195, 2, 'Boudewijn van Immerseel heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-17 09:06:16', 8);
INSERT INTO `logs` VALUES (196, 2, 'Boudewijn van Immerseel heeft het veld Einddatum huidige ovk gewijzigd', '2021-08-17 09:06:16', 8);
INSERT INTO `logs` VALUES (197, 2, 'Boudewijn van Immerseel heeft het veld Datum publicatie gewijzigd', '2021-08-17 09:06:16', 8);
INSERT INTO `logs` VALUES (198, 2, 'Boudewijn van Immerseel heeft het veld Datum uiterste inschrijving gewijzigd', '2021-08-17 09:06:16', 8);
INSERT INTO `logs` VALUES (199, 2, 'Boudewijn van Immerseel heeft het veld Datum gunningsbeslissing gewijzigd', '2021-08-17 09:06:16', 8);
INSERT INTO `logs` VALUES (200, 2, 'Boudewijn van Immerseel heeft het veld Status gewijzigd', '2021-08-17 09:06:58', 8);
INSERT INTO `logs` VALUES (201, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-08-17 13:44:37', 28);
INSERT INTO `logs` VALUES (202, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (203, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (204, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (205, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (206, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (207, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (208, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (209, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-08-24 12:18:37', NULL);
INSERT INTO `logs` VALUES (210, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-24 12:19:07', 38);
INSERT INTO `logs` VALUES (211, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-24 12:22:05', 23);
INSERT INTO `logs` VALUES (212, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-24 12:23:07', 23);
INSERT INTO `logs` VALUES (213, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (214, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (215, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (216, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (217, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (218, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (219, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (220, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (221, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-08-26 09:44:56', NULL);
INSERT INTO `logs` VALUES (222, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-08-26 09:47:11', 39);
INSERT INTO `logs` VALUES (223, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-08-26 09:47:11', 39);
INSERT INTO `logs` VALUES (224, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-08-26 09:47:11', 39);
INSERT INTO `logs` VALUES (225, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-08-26 09:47:11', 39);
INSERT INTO `logs` VALUES (226, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-08-26 09:47:11', 39);
INSERT INTO `logs` VALUES (227, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (228, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (229, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (230, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (231, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (232, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (233, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (234, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (235, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-08-30 13:08:45', NULL);
INSERT INTO `logs` VALUES (236, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-30 13:18:18', 40);
INSERT INTO `logs` VALUES (237, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (238, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (239, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (240, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (241, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (242, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (243, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-08-31 07:59:42', NULL);
INSERT INTO `logs` VALUES (244, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-08-31 08:01:27', 41);
INSERT INTO `logs` VALUES (245, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-08-31 13:04:36', 17);
INSERT INTO `logs` VALUES (246, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (247, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (248, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (249, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (250, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (251, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (252, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (253, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (254, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-09-06 07:59:07', NULL);
INSERT INTO `logs` VALUES (255, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-14 07:03:48', NULL);
INSERT INTO `logs` VALUES (256, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-14 07:03:48', NULL);
INSERT INTO `logs` VALUES (257, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-14 07:03:48', NULL);
INSERT INTO `logs` VALUES (258, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-14 07:03:48', NULL);
INSERT INTO `logs` VALUES (259, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-14 07:03:48', NULL);
INSERT INTO `logs` VALUES (260, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-14 07:03:48', NULL);
INSERT INTO `logs` VALUES (261, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-14 07:04:36', 43);
INSERT INTO `logs` VALUES (262, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-14 07:04:36', 43);
INSERT INTO `logs` VALUES (263, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-14 07:06:08', 30);
INSERT INTO `logs` VALUES (264, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (265, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (266, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (267, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (268, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (269, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (270, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-14 08:55:37', NULL);
INSERT INTO `logs` VALUES (271, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-14 08:58:10', 44);
INSERT INTO `logs` VALUES (272, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-09-14 10:13:35', 44);
INSERT INTO `logs` VALUES (273, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (274, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (275, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (276, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (277, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (278, 21, 'Rens Nibbeling heeft het veld Meelezer gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (279, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (280, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (281, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (282, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (283, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (284, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (285, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-16 08:14:24', NULL);
INSERT INTO `logs` VALUES (286, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-09-16 08:15:46', 45);
INSERT INTO `logs` VALUES (287, 21, 'Rens Nibbeling heeft het veld Meelezer gewijzigd', '2021-09-16 12:57:32', 45);
INSERT INTO `logs` VALUES (288, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-09-16 12:57:32', 45);
INSERT INTO `logs` VALUES (289, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-09-16 12:59:38', 45);
INSERT INTO `logs` VALUES (290, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-09-24 09:43:48', 1);
INSERT INTO `logs` VALUES (291, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (292, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (293, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (294, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (295, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (296, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (297, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (298, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-27 10:22:09', NULL);
INSERT INTO `logs` VALUES (299, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (300, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (301, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (302, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (303, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (304, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (305, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (306, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-27 10:25:13', NULL);
INSERT INTO `logs` VALUES (307, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (308, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (309, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (310, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (311, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (312, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (313, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (314, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-27 10:26:44', NULL);
INSERT INTO `logs` VALUES (315, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-09-27 10:28:54', 47);
INSERT INTO `logs` VALUES (316, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-09-27 10:29:13', 46);
INSERT INTO `logs` VALUES (317, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (318, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (319, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (320, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (321, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (322, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (323, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-09-27 10:30:47', NULL);
INSERT INTO `logs` VALUES (324, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-04 13:19:07', 5);
INSERT INTO `logs` VALUES (325, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-04 13:19:48', 5);
INSERT INTO `logs` VALUES (326, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:10:54', 1);
INSERT INTO `logs` VALUES (327, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:11:50', 3);
INSERT INTO `logs` VALUES (328, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:12:46', 23);
INSERT INTO `logs` VALUES (329, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:12:59', 38);
INSERT INTO `logs` VALUES (330, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 09:16:28', 42);
INSERT INTO `logs` VALUES (331, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 09:16:28', 42);
INSERT INTO `logs` VALUES (332, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 09:16:28', 42);
INSERT INTO `logs` VALUES (333, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-10-06 09:16:28', 42);
INSERT INTO `logs` VALUES (334, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 09:16:28', 42);
INSERT INTO `logs` VALUES (335, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 09:18:25', 42);
INSERT INTO `logs` VALUES (336, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 09:19:17', 42);
INSERT INTO `logs` VALUES (337, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:20:14', 2);
INSERT INTO `logs` VALUES (338, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:22:06', 4);
INSERT INTO `logs` VALUES (339, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:22:39', 5);
INSERT INTO `logs` VALUES (340, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-10-06 09:25:34', 8);
INSERT INTO `logs` VALUES (341, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:25:54', 8);
INSERT INTO `logs` VALUES (342, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:29:20', 6);
INSERT INTO `logs` VALUES (343, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:30:25', 7);
INSERT INTO `logs` VALUES (344, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:30:34', 9);
INSERT INTO `logs` VALUES (345, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:30:42', 11);
INSERT INTO `logs` VALUES (346, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:30:54', 12);
INSERT INTO `logs` VALUES (347, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:31:06', 16);
INSERT INTO `logs` VALUES (348, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:31:20', 31);
INSERT INTO `logs` VALUES (349, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:31:32', 39);
INSERT INTO `logs` VALUES (350, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:31:45', 40);
INSERT INTO `logs` VALUES (351, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:31:59', 48);
INSERT INTO `logs` VALUES (352, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 09:32:07', 49);
INSERT INTO `logs` VALUES (353, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 09:54:52', 21);
INSERT INTO `logs` VALUES (354, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 11:07:09', 24);
INSERT INTO `logs` VALUES (355, 21, 'Rens Nibbeling heeft het veld Contractmanager gewijzigd', '2021-10-06 11:07:09', 24);
INSERT INTO `logs` VALUES (356, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-10-06 11:07:09', 24);
INSERT INTO `logs` VALUES (357, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 11:10:55', 24);
INSERT INTO `logs` VALUES (358, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 11:10:55', 24);
INSERT INTO `logs` VALUES (359, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-10-06 11:10:55', 24);
INSERT INTO `logs` VALUES (360, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 11:10:55', 24);
INSERT INTO `logs` VALUES (361, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-10-06 11:10:55', 24);
INSERT INTO `logs` VALUES (362, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 11:10:55', 24);
INSERT INTO `logs` VALUES (363, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 11:11:22', 24);
INSERT INTO `logs` VALUES (364, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 11:11:30', 24);
INSERT INTO `logs` VALUES (365, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 11:15:00', 26);
INSERT INTO `logs` VALUES (366, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 11:15:00', 26);
INSERT INTO `logs` VALUES (367, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-10-06 11:15:00', 26);
INSERT INTO `logs` VALUES (368, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 11:15:00', 26);
INSERT INTO `logs` VALUES (369, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-06 11:15:00', 26);
INSERT INTO `logs` VALUES (370, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 11:22:44', 26);
INSERT INTO `logs` VALUES (371, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 11:23:47', 26);
INSERT INTO `logs` VALUES (372, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-06 11:29:17', 26);
INSERT INTO `logs` VALUES (373, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 11:30:17', 26);
INSERT INTO `logs` VALUES (374, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 11:34:36', 26);
INSERT INTO `logs` VALUES (375, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-10-06 11:34:36', 26);
INSERT INTO `logs` VALUES (376, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-10-06 11:37:40', 26);
INSERT INTO `logs` VALUES (377, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 11:39:15', 26);
INSERT INTO `logs` VALUES (378, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 11:40:55', 26);
INSERT INTO `logs` VALUES (379, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 11:40:55', 26);
INSERT INTO `logs` VALUES (380, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 11:40:55', 26);
INSERT INTO `logs` VALUES (381, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-06 11:40:55', 26);
INSERT INTO `logs` VALUES (382, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 11:49:05', 26);
INSERT INTO `logs` VALUES (383, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 11:54:18', 27);
INSERT INTO `logs` VALUES (384, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 11:54:18', 27);
INSERT INTO `logs` VALUES (385, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-10-06 11:54:18', 27);
INSERT INTO `logs` VALUES (386, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 11:54:18', 27);
INSERT INTO `logs` VALUES (387, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-06 11:54:18', 27);
INSERT INTO `logs` VALUES (388, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 11:58:34', 27);
INSERT INTO `logs` VALUES (389, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 12:02:25', 26);
INSERT INTO `logs` VALUES (390, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 12:02:25', 26);
INSERT INTO `logs` VALUES (391, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 12:02:25', 26);
INSERT INTO `logs` VALUES (392, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-06 12:02:25', 26);
INSERT INTO `logs` VALUES (393, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:03:58', 27);
INSERT INTO `logs` VALUES (394, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:04:13', 27);
INSERT INTO `logs` VALUES (395, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:06:31', 27);
INSERT INTO `logs` VALUES (396, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 12:06:31', 27);
INSERT INTO `logs` VALUES (397, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-10-06 12:06:31', 27);
INSERT INTO `logs` VALUES (398, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:07:22', 27);
INSERT INTO `logs` VALUES (399, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:07:45', 27);
INSERT INTO `logs` VALUES (400, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:12:08', 27);
INSERT INTO `logs` VALUES (401, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 12:12:08', 27);
INSERT INTO `logs` VALUES (402, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:13:18', 27);
INSERT INTO `logs` VALUES (403, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:16:33', 26);
INSERT INTO `logs` VALUES (404, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:16:59', 26);
INSERT INTO `logs` VALUES (405, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:17:18', 26);
INSERT INTO `logs` VALUES (406, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:27:39', 18);
INSERT INTO `logs` VALUES (407, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 12:27:39', 18);
INSERT INTO `logs` VALUES (408, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:28:13', 18);
INSERT INTO `logs` VALUES (409, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 12:29:24', 18);
INSERT INTO `logs` VALUES (410, 21, 'Rens Nibbeling heeft het veld Einddatum huidige ovk gewijzigd', '2021-10-06 12:30:57', 18);
INSERT INTO `logs` VALUES (411, 21, 'Rens Nibbeling heeft het veld Afdeling gewijzigd', '2021-10-06 12:30:57', 18);
INSERT INTO `logs` VALUES (412, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:33:13', 18);
INSERT INTO `logs` VALUES (413, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:38:01', NULL);
INSERT INTO `logs` VALUES (414, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:38:01', NULL);
INSERT INTO `logs` VALUES (415, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:38:01', NULL);
INSERT INTO `logs` VALUES (416, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:38:01', NULL);
INSERT INTO `logs` VALUES (417, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 12:38:01', NULL);
INSERT INTO `logs` VALUES (418, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:38:01', NULL);
INSERT INTO `logs` VALUES (419, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:38:47', NULL);
INSERT INTO `logs` VALUES (420, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:38:47', NULL);
INSERT INTO `logs` VALUES (421, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:38:47', NULL);
INSERT INTO `logs` VALUES (422, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:38:47', NULL);
INSERT INTO `logs` VALUES (423, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 12:38:47', NULL);
INSERT INTO `logs` VALUES (424, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:38:47', NULL);
INSERT INTO `logs` VALUES (425, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:39:49', NULL);
INSERT INTO `logs` VALUES (426, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:39:49', NULL);
INSERT INTO `logs` VALUES (427, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:39:49', NULL);
INSERT INTO `logs` VALUES (428, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:39:49', NULL);
INSERT INTO `logs` VALUES (429, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 12:39:49', NULL);
INSERT INTO `logs` VALUES (430, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:39:49', NULL);
INSERT INTO `logs` VALUES (431, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:42:08', 17);
INSERT INTO `logs` VALUES (432, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:42:08', 17);
INSERT INTO `logs` VALUES (433, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:42:54', 17);
INSERT INTO `logs` VALUES (434, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:42:54', 17);
INSERT INTO `logs` VALUES (435, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (436, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (437, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (438, 21, 'Rens Nibbeling heeft het veld Datum publicatie gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (439, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (440, 21, 'Rens Nibbeling heeft het veld Uiterste datum stellen vragen NvI 2 gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (441, 21, 'Rens Nibbeling heeft het veld Datum uiterste inschrijving gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (442, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (443, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:44:47', 24);
INSERT INTO `logs` VALUES (444, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (445, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (446, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (447, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (448, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (449, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (450, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:49:01', NULL);
INSERT INTO `logs` VALUES (451, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 12:55:37', 35);
INSERT INTO `logs` VALUES (452, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 12:59:36', NULL);
INSERT INTO `logs` VALUES (453, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 12:59:36', NULL);
INSERT INTO `logs` VALUES (454, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 12:59:36', NULL);
INSERT INTO `logs` VALUES (455, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 12:59:36', NULL);
INSERT INTO `logs` VALUES (456, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 12:59:36', NULL);
INSERT INTO `logs` VALUES (457, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 12:59:36', NULL);
INSERT INTO `logs` VALUES (458, 21, 'Rens Nibbeling heeft het traject verwijderd', '2021-10-06 12:59:53', 54);
INSERT INTO `logs` VALUES (459, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (460, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (461, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (462, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (463, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (464, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (465, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 14:33:43', NULL);
INSERT INTO `logs` VALUES (466, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (467, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (468, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (469, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (470, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (471, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (472, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 14:38:47', NULL);
INSERT INTO `logs` VALUES (473, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (474, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (475, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (476, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (477, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (478, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (479, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 14:51:20', NULL);
INSERT INTO `logs` VALUES (480, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (481, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (482, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (483, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (484, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (485, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (486, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 14:54:49', NULL);
INSERT INTO `logs` VALUES (487, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (488, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (489, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (490, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (491, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (492, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (493, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 14:57:12', NULL);
INSERT INTO `logs` VALUES (494, 21, 'Rens Nibbeling heeft het veld Naam traject gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (495, 21, 'Rens Nibbeling heeft het veld Procedure gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (496, 21, 'Rens Nibbeling heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (497, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (498, 21, 'Rens Nibbeling heeft het veld Inkoper gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (499, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (500, 21, 'Rens Nibbeling heeft het veld Inzet inkoper gewijzigd', '2021-10-06 14:59:12', NULL);
INSERT INTO `logs` VALUES (501, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-07 09:43:14', 30);
INSERT INTO `logs` VALUES (502, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-07 09:44:08', 17);
INSERT INTO `logs` VALUES (503, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-07 12:14:45', 30);
INSERT INTO `logs` VALUES (504, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-07 12:15:06', 30);
INSERT INTO `logs` VALUES (505, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-07 13:14:21', 30);
INSERT INTO `logs` VALUES (506, 21, 'Rens Nibbeling heeft het veld Datum gunningsbeslissing gewijzigd', '2021-10-07 13:16:32', 30);
INSERT INTO `logs` VALUES (507, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-07 13:17:52', 30);
INSERT INTO `logs` VALUES (508, 21, 'Rens Nibbeling heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-07 13:18:11', 30);
INSERT INTO `logs` VALUES (509, 21, 'Rens Nibbeling heeft het veld Status gewijzigd', '2021-10-07 13:20:11', 21);
INSERT INTO `logs` VALUES (510, 27, 'Jasper van Tetering heeft het veld Opmerkingen gewijzigd', '2021-10-11 09:48:14', 57);
INSERT INTO `logs` VALUES (511, 27, 'Jasper van Tetering heeft het veld Status gewijzigd', '2021-10-11 09:55:47', 57);
INSERT INTO `logs` VALUES (512, 27, 'Jasper van Tetering heeft het veld Status gewijzigd', '2021-10-11 09:55:58', 57);
INSERT INTO `logs` VALUES (513, 27, 'Jasper van Tetering heeft het veld Status gewijzigd', '2021-10-11 09:56:15', 57);
INSERT INTO `logs` VALUES (514, 27, 'Jasper van Tetering heeft het veld Meelezer gewijzigd', '2021-10-12 10:29:14', 17);
INSERT INTO `logs` VALUES (515, 27, 'Jasper van Tetering heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-18 11:50:38', 18);
INSERT INTO `logs` VALUES (516, 27, 'Jasper van Tetering heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-10-18 11:50:58', 18);
INSERT INTO `logs` VALUES (517, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-10-18 11:52:16', 60);
INSERT INTO `logs` VALUES (518, 2, 'Boudewijn van Immerseel heeft het veld Opmerkingen gewijzigd', '2021-10-18 13:02:42', 45);
INSERT INTO `logs` VALUES (519, 2, 'Boudewijn van Immerseel heeft het veld Opmerkingen gewijzigd', '2021-10-18 13:03:06', 45);
INSERT INTO `logs` VALUES (520, 1, 'Radek Dolewa heeft het veld Inzet inkoper gewijzigd', '2021-10-19 07:56:28', 18);
INSERT INTO `logs` VALUES (521, 1, 'Radek Dolewa heeft het veld Inzet inkoper gewijzigd', '2021-10-19 09:04:05', 18);
INSERT INTO `logs` VALUES (522, 1, 'Radek Dolewa heeft het veld Inzet inkoper gewijzigd', '2021-10-19 11:03:27', 18);
INSERT INTO `logs` VALUES (523, 21, 'Rens Nibbeling heeft het veld Opmerkingen gewijzigd', '2021-10-19 16:43:38', 60);
INSERT INTO `logs` VALUES (524, 2, 'Boudewijn van Immerseel heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-10-27 15:05:32', 17);
INSERT INTO `logs` VALUES (525, 2, 'Boudewijn van Immerseel heeft het veld Status gewijzigd', '2021-10-27 15:06:53', 17);
INSERT INTO `logs` VALUES (526, 2, 'Boudewijn van Immerseel heeft het veld Status gewijzigd', '2021-10-27 15:07:12', 17);
INSERT INTO `logs` VALUES (527, 27, 'Jasper van Tetering heeft het veld Naam traject gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (528, 27, 'Jasper van Tetering heeft het veld Procedure gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (529, 27, 'Jasper van Tetering heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (530, 27, 'Jasper van Tetering heeft het veld Status gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (531, 27, 'Jasper van Tetering heeft het veld Contractmanager gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (532, 27, 'Jasper van Tetering heeft het veld Inkoper gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (533, 27, 'Jasper van Tetering heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (534, 27, 'Jasper van Tetering heeft het veld Inzet inkoper gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (535, 27, 'Jasper van Tetering heeft het veld Afdeling gewijzigd', '2021-11-02 16:55:45', NULL);
INSERT INTO `logs` VALUES (536, 27, 'Jasper van Tetering heeft het veld Datum publicatie gewijzigd', '2021-11-02 16:56:56', 61);
INSERT INTO `logs` VALUES (537, 27, 'Jasper van Tetering heeft het veld Datum uiterste inschrijving gewijzigd', '2021-11-02 16:56:56', 61);
INSERT INTO `logs` VALUES (538, 27, 'Jasper van Tetering heeft het veld Datum gunningsbeslissing gewijzigd', '2021-11-02 16:56:56', 61);
INSERT INTO `logs` VALUES (539, 27, 'Jasper van Tetering heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-11-03 14:51:51', 15);
INSERT INTO `logs` VALUES (540, 27, 'Jasper van Tetering heeft het veld Status gewijzigd', '2021-11-03 14:52:19', 15);
INSERT INTO `logs` VALUES (541, 27, 'Jasper van Tetering heeft het veld Verwachte ingangsdatum ovk gewijzigd', '2021-11-03 14:54:30', 15);
INSERT INTO `logs` VALUES (542, 27, 'Jasper van Tetering heeft het veld Datum publicatie gewijzigd', '2021-11-03 14:55:06', 15);
INSERT INTO `logs` VALUES (543, 27, 'Jasper van Tetering heeft het veld Datum uiterste inschrijving gewijzigd', '2021-11-03 14:55:06', 15);
INSERT INTO `logs` VALUES (544, 27, 'Jasper van Tetering heeft het veld Datum gunningsbeslissing gewijzigd', '2021-11-03 14:55:06', 15);
INSERT INTO `logs` VALUES (545, 27, 'Jasper van Tetering heeft het veld Inzet inkoper gewijzigd', '2021-11-04 09:06:27', 17);
INSERT INTO `logs` VALUES (546, 27, 'Jasper van Tetering heeft het veld Inkoper gewijzigd', '2021-11-04 09:32:13', 61);
INSERT INTO `logs` VALUES (547, 27, 'Jasper van Tetering heeft het veld Procedure gewijzigd', '2021-11-04 14:07:39', 17);
INSERT INTO `logs` VALUES (548, 27, 'Jasper van Tetering heeft het veld Doorlooptijd procedure (maanden) gewijzigd', '2021-11-04 14:07:39', 17);
INSERT INTO `logs` VALUES (549, 27, 'Jasper van Tetering heeft het veld Contractmanager gewijzigd', '2021-11-04 14:07:39', 17);
INSERT INTO `logs` VALUES (550, 27, 'Jasper van Tetering heeft het veld Uiterste datum stellen vragen NvI 1 gewijzigd', '2021-11-04 14:14:03', 17);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2020_04_30_134015_create_roles_table', 1);
INSERT INTO `migrations` VALUES (5, '2020_05_05_120449_create_traject_table', 1);
INSERT INTO `migrations` VALUES (6, '2020_08_13_093137_create_form_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
INSERT INTO `password_resets` VALUES ('info@procureplan.nl', '$2y$10$qpDx82/2Fhy/Bp3V8amAFOEJaaUookpWvR6d/thZeRDE857e1lo22', '2020-11-25 10:02:18');
INSERT INTO `password_resets` VALUES ('dev@procureplan.nl', '$2y$10$zcVKRp2xAPdlMrwGkU.UPeOqvyRDZLRjuFIx.J/Qwc3f38G4AyKEK', '2020-11-25 10:03:00');
INSERT INTO `password_resets` VALUES ('bvanimmerseel@procurance.nl', '$2y$10$chPbuI6jz6FSXhlxQjXxZe2yjzIdz9aJME13GhlVDodFEsewYfBTK', '2020-11-25 10:03:23');
INSERT INTO `password_resets` VALUES ('servicedesk@procureplan.nl', '$2y$10$f98H9scL2Q0P5vJhkTL9t.rL1bTsjtY909xWldeX6MkkB3470.JXq', '2020-11-25 10:04:53');
INSERT INTO `password_resets` VALUES ('jennifer.wijnen@uwv.nl', '$2y$10$t7ED6TWL1aZ9bt6f4t9NdeWwVtXiHd4iFDLuRlgoFsvvppnVerGPC', '2021-02-05 13:47:21');
INSERT INTO `password_resets` VALUES ('testaccount@procureplan.nl', '$2y$10$3xHr0TirYkfL3leMePOEtOVFz.8IufPTN4.nIzeMHnTIWvcadKX3W', '2021-09-14 08:41:37');
COMMIT;

-- ----------------------------
-- Table structure for password_securities
-- ----------------------------
DROP TABLE IF EXISTS `password_securities`;
CREATE TABLE `password_securities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `password_expiry_days` int(11) DEFAULT NULL,
  `password_updated_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of password_securities
-- ----------------------------
BEGIN;
INSERT INTO `password_securities` VALUES (1, 1, 180, '2021-06-23 14:31:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (2, 2, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (3, 3, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (4, 4, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (5, 5, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (6, 6, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (7, 7, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (8, 8, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (9, 9, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (10, 10, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (11, 11, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (12, 12, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (13, 13, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (14, 14, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (21, 21, 180, '2021-08-01 00:00:23', NULL, NULL);
INSERT INTO `password_securities` VALUES (22, 25, 180, '2021-08-16 14:40:55', '2021-08-16 14:40:55', '2021-08-16 14:40:55');
INSERT INTO `password_securities` VALUES (23, 26, 180, '2021-09-14 08:41:37', '2021-09-14 08:41:37', '2021-09-14 08:41:37');
INSERT INTO `password_securities` VALUES (24, 27, 180, '2021-10-04 11:29:34', '2021-10-04 11:29:34', '2021-10-04 11:29:34');
COMMIT;

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `feedback` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of ratings
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
BEGIN;
INSERT INTO `role_user` VALUES (1, 4, '2021-01-12 09:07:41', '2021-01-12 09:07:41');
INSERT INTO `role_user` VALUES (1, 5, '2021-01-12 09:07:41', '2021-01-12 09:07:41');
INSERT INTO `role_user` VALUES (1, 7, '2021-01-12 09:07:41', '2021-01-12 09:07:41');
INSERT INTO `role_user` VALUES (2, 4, '2021-10-06 12:51:43', '2021-10-06 12:51:43');
INSERT INTO `role_user` VALUES (2, 5, '2021-10-06 12:51:43', '2021-10-06 12:51:43');
INSERT INTO `role_user` VALUES (2, 7, '2021-10-06 12:51:43', '2021-10-06 12:51:43');
INSERT INTO `role_user` VALUES (3, 2, '2021-10-06 12:54:02', '2021-10-06 12:54:02');
INSERT INTO `role_user` VALUES (3, 5, '2021-10-06 12:54:02', '2021-10-06 12:54:02');
INSERT INTO `role_user` VALUES (4, 5, '2021-05-27 11:37:29', '2021-05-27 11:37:29');
INSERT INTO `role_user` VALUES (15, 1, '2020-11-25 10:03:00', '2020-11-25 10:03:00');
INSERT INTO `role_user` VALUES (16, 3, '2020-11-25 10:03:23', '2020-11-25 10:03:23');
INSERT INTO `role_user` VALUES (17, 4, '2020-11-25 10:04:53', '2020-11-25 10:04:53');
INSERT INTO `role_user` VALUES (18, 3, '2021-10-06 12:52:47', '2021-10-06 12:52:47');
INSERT INTO `role_user` VALUES (18, 4, '2021-10-06 12:52:47', '2021-10-06 12:52:47');
INSERT INTO `role_user` VALUES (18, 5, '2021-10-06 12:52:47', '2021-10-06 12:52:47');
INSERT INTO `role_user` VALUES (18, 6, '2021-10-06 12:52:47', '2021-10-06 12:52:47');
INSERT INTO `role_user` VALUES (19, 4, '2021-03-04 10:11:40', '2021-03-04 10:11:40');
INSERT INTO `role_user` VALUES (19, 5, '2021-03-04 10:11:40', '2021-03-04 10:11:40');
INSERT INTO `role_user` VALUES (19, 6, '2021-03-04 10:11:40', '2021-03-04 10:11:40');
INSERT INTO `role_user` VALUES (20, 4, '2021-03-04 10:11:45', '2021-03-04 10:11:45');
INSERT INTO `role_user` VALUES (20, 5, '2021-03-04 10:11:45', '2021-03-04 10:11:45');
INSERT INTO `role_user` VALUES (21, 3, '2021-10-14 09:29:17', '2021-10-14 09:29:17');
INSERT INTO `role_user` VALUES (21, 4, '2021-10-14 09:29:17', '2021-10-14 09:29:17');
INSERT INTO `role_user` VALUES (21, 5, '2021-10-14 09:29:17', '2021-10-14 09:29:17');
INSERT INTO `role_user` VALUES (21, 6, '2021-10-14 09:29:17', '2021-10-14 09:29:17');
INSERT INTO `role_user` VALUES (23, 4, '2021-06-01 13:46:20', '2021-06-01 13:46:20');
INSERT INTO `role_user` VALUES (23, 5, '2021-06-01 13:46:20', '2021-06-01 13:46:20');
INSERT INTO `role_user` VALUES (25, 3, '2021-08-25 11:21:08', '2021-08-25 11:21:08');
INSERT INTO `role_user` VALUES (25, 4, '2021-08-25 11:21:08', '2021-08-25 11:21:08');
INSERT INTO `role_user` VALUES (25, 5, '2021-08-25 11:21:08', '2021-08-25 11:21:08');
INSERT INTO `role_user` VALUES (26, 2, '2021-11-04 09:34:50', '2021-11-04 09:34:50');
INSERT INTO `role_user` VALUES (27, 1, '2021-10-13 07:04:42', '2021-10-13 07:04:42');
INSERT INTO `role_user` VALUES (27, 4, '2021-10-13 07:04:42', '2021-10-13 07:04:42');
INSERT INTO `role_user` VALUES (27, 5, '2021-10-13 07:04:42', '2021-10-13 07:04:42');
INSERT INTO `role_user` VALUES (27, 6, '2021-10-13 07:04:42', '2021-10-13 07:04:42');
INSERT INTO `role_user` VALUES (27, 7, '2021-10-13 07:04:42', '2021-10-13 07:04:42');
COMMIT;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, 'lawyer', 'Jurist', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `roles` VALUES (2, 'ro', 'Alleen Lezen', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `roles` VALUES (3, 'clm', 'Contractmanager', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `roles` VALUES (4, 'reader', 'Meelezer', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `roles` VALUES (5, 'buyer', 'Inkoper', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `roles` VALUES (6, 'admin', 'Beheerder', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `roles` VALUES (7, 'superAdmin', 'Backend Beheerder', '2020-11-10 08:50:57', '2020-11-10 08:50:57');
COMMIT;

-- ----------------------------
-- Table structure for session_search
-- ----------------------------
DROP TABLE IF EXISTS `session_search`;
CREATE TABLE `session_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search` text COLLATE utf8mb4_0900_ai_ci,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of session_search
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for traject_procedures
-- ----------------------------
DROP TABLE IF EXISTS `traject_procedures`;
CREATE TABLE `traject_procedures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visible` int(11) DEFAULT NULL,
  `publication_phase` int(11) DEFAULT NULL,
  `assessment_phase` int(11) DEFAULT NULL,
  `objection_phase` int(11) DEFAULT NULL,
  `lead_period` int(11) DEFAULT NULL,
  `buyer_hours` int(11) DEFAULT NULL,
  `prepare_phase_percentage` int(11) DEFAULT NULL,
  `publication_phase_percentage` int(11) DEFAULT NULL,
  `publication_phase_nvl_one_percentage` int(11) DEFAULT NULL,
  `publication_phase_nvl_two_percentage` int(11) DEFAULT NULL,
  `assessment_phase_phase_percentage` int(11) DEFAULT NULL,
  `objection_phase_phase_percentage` int(11) DEFAULT NULL,
  `implementation_phase_percentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of traject_procedures
-- ----------------------------
BEGIN;
INSERT INTO `traject_procedures` VALUES (1, 'Enkelvoudig onderhands', 1, 10, 10, 0, 1, 20, 60, 15, 0, 0, 20, 0, 5, '2020-11-10 08:50:57', '2021-07-26 13:18:48', NULL);
INSERT INTO `traject_procedures` VALUES (2, 'Meervoudig onderhands', 1, 30, 14, 10, 6, 100, 40, 20, 0, 0, 20, 10, 10, '2020-11-10 08:50:57', '2020-11-18 11:58:47', NULL);
INSERT INTO `traject_procedures` VALUES (3, 'Europese aanbesteding', 1, 40, 20, 21, 12, 200, 30, 30, 60, 40, 30, 5, 5, '2020-11-10 08:50:57', '2021-08-19 10:27:48', NULL);
INSERT INTO `traject_procedures` VALUES (4, 'Minicompetitie', 1, 5, 5, 5, 1, 10, 40, 10, 0, 0, 40, 5, 5, '2020-11-10 08:50:57', '2021-10-06 09:41:39', NULL);
INSERT INTO `traject_procedures` VALUES (5, 'Europees complex', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-18 11:56:29', '2021-06-18 09:45:42', NULL);
INSERT INTO `traject_procedures` VALUES (6, 'Offerteaanvraag complex', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-19 12:54:59', '2021-10-12 14:48:43', NULL);
INSERT INTO `traject_procedures` VALUES (7, 'Europees eenvoudig', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 12:27:42', '2021-06-21 12:45:17', NULL);
INSERT INTO `traject_procedures` VALUES (8, 'Europees regulier', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-25 11:15:32', '2021-10-06 09:39:10', NULL);
INSERT INTO `traject_procedures` VALUES (9, 'Europees niet-openbaar', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-25 11:17:30', '2020-11-25 11:17:30', NULL);
INSERT INTO `traject_procedures` VALUES (10, 'Offerteaanvraag complex', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 13:19:16', '2021-10-06 09:39:17', NULL);
INSERT INTO `traject_procedures` VALUES (11, 'Besluit over verlenging', 0, NULL, NULL, NULL, 1, NULL, 40, 40, 50, 50, 5, 5, 10, '2021-02-08 14:02:02', '2021-10-06 09:40:46', NULL);
INSERT INTO `traject_procedures` VALUES (12, 'Nationaal openbaar', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-23 09:21:15', '2021-02-23 09:21:15', NULL);
INSERT INTO `traject_procedures` VALUES (13, 'Meelezen met collega', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-30 08:58:55', '2021-10-06 09:40:58', NULL);
INSERT INTO `traject_procedures` VALUES (14, 'Subsidie', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-19 08:06:05', '2021-06-23 14:03:53', NULL);
INSERT INTO `traject_procedures` VALUES (15, 'Complex test', 0, NULL, NULL, NULL, 12, 240, 20, 20, 60, 40, 20, 20, 20, '2021-08-26 09:40:59', '2021-10-06 09:41:02', NULL);
INSERT INTO `traject_procedures` VALUES (16, 'Beslissen over verlengen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-30 13:05:43', '2021-08-30 13:06:00', NULL);
INSERT INTO `traject_procedures` VALUES (99, 'Intern Project', 1, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for traject_statuses
-- ----------------------------
DROP TABLE IF EXISTS `traject_statuses`;
CREATE TABLE `traject_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of traject_statuses
-- ----------------------------
BEGIN;
INSERT INTO `traject_statuses` VALUES (1, 'Idee', NULL, 1, '2020-11-10 08:50:57', '2021-11-04 08:40:49');
INSERT INTO `traject_statuses` VALUES (2, 'Koppelen inkoper', NULL, 1, '2020-11-10 08:50:57', '2021-06-14 10:41:53');
INSERT INTO `traject_statuses` VALUES (3, 'Voorbereiden', NULL, 1, '2020-11-10 08:50:57', '2021-11-04 14:32:58');
INSERT INTO `traject_statuses` VALUES (4, 'Gepubliceerd/loopt', NULL, 1, '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `traject_statuses` VALUES (5, 'Beoordelingsfase', NULL, 1, '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `traject_statuses` VALUES (6, 'Gunningsfase en standstill', NULL, 1, '2020-11-10 08:50:57', '2021-06-02 11:24:15');
INSERT INTO `traject_statuses` VALUES (7, 'Afgerond', NULL, 0, '2020-11-10 08:50:57', '2021-11-04 08:45:52');
INSERT INTO `traject_statuses` VALUES (8, 'On Hold', NULL, 1, '2020-11-10 08:50:57', '2021-07-26 13:18:31');
INSERT INTO `traject_statuses` VALUES (9, 'Teruggetrokken', NULL, 1, '2020-11-10 08:50:57', '2021-10-06 09:55:13');
INSERT INTO `traject_statuses` VALUES (10, 'Onbekend', NULL, 1, '2020-11-10 08:50:57', '2020-11-10 08:50:57');
INSERT INTO `traject_statuses` VALUES (17, 'Afgerond en dossier compleet', 7, 1, '2021-06-01 13:15:38', '2021-06-01 13:15:38');
INSERT INTO `traject_statuses` VALUES (20, 'Afgerond', NULL, 0, '2021-06-30 06:44:07', '2021-11-04 08:46:28');
COMMIT;

-- ----------------------------
-- Table structure for trajects
-- ----------------------------
DROP TABLE IF EXISTS `trajects`;
CREATE TABLE `trajects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` int(11) DEFAULT NULL,
  `procedure_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `clm_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `reader_id` int(11) DEFAULT NULL,
  `legal_id` int(11) DEFAULT NULL,
  `start_contract` date DEFAULT NULL,
  `end_contract` date DEFAULT NULL,
  `publication` date DEFAULT NULL,
  `max_subscribe` date DEFAULT NULL,
  `nomination` date DEFAULT NULL,
  `nvi_one` date DEFAULT NULL,
  `nvi_two` date DEFAULT NULL,
  `clm_cap` int(11) DEFAULT NULL,
  `buyer_cap` int(11) DEFAULT NULL,
  `reader_cap` int(11) DEFAULT NULL,
  `legal_cap` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `feedback` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `traject_start` date DEFAULT NULL,
  `custom_603cbe0a12b29` longtext COLLATE utf8mb4_unicode_ci,
  `custom_5fd14227b83f1` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of trajects
-- ----------------------------
BEGIN;
INSERT INTO `trajects` VALUES (1, 'test', 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2020-11-10 10:14:28', '2021-10-06 09:10:54', '2021-10-06 09:10:54', 'excellent', NULL, 'Testopmerking - 24-09-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (2, 'EA Regulier geen risico 1', 12, 3, 17, 16, 4, 20, 15, '2021-11-01', '2021-08-31', '2021-06-01', '2021-07-15', '2021-08-15', '2021-06-14', '2021-06-21', NULL, 80, NULL, NULL, '2020-11-12 07:46:12', '2021-10-06 09:20:14', '2021-10-06 09:20:14', 'hate', '2020-11-01', NULL, NULL);
INSERT INTO `trajects` VALUES (3, 'EA Regulier geen risico 2', 8, 3, 16, NULL, 2, 21, 15, '2022-09-01', '2022-08-31', '2022-03-01', '2022-05-01', '2022-06-10', '2022-03-10', '2022-03-20', NULL, 200, NULL, NULL, '2020-11-12 07:46:53', '2021-10-06 09:11:50', '2021-10-06 09:11:50', 'hate', '2022-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (4, 'EA Strategisch geen risico 1', 10, 3, 3, NULL, 4, NULL, NULL, '2022-08-25', '2022-08-24', '2022-09-12', NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2020-11-12 07:47:26', '2021-10-06 09:22:06', '2021-10-06 09:22:06', 'disappointed', '2021-10-25', NULL, NULL);
INSERT INTO `trajects` VALUES (5, 'MOP geen risico 1', 6, 2, 8, NULL, 1, 18, NULL, '2021-12-01', '2021-11-30', NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2020-11-12 07:48:09', '2021-10-06 09:22:39', '2021-10-06 09:22:39', 'excellent', '2021-06-01', NULL, NULL);
INSERT INTO `trajects` VALUES (6, 'EOP geen risico 1', 1, 1, 1, NULL, 2, NULL, NULL, '2021-11-01', '2021-10-31', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2020-11-12 07:49:05', '2021-10-06 09:29:20', '2021-10-06 09:29:20', 'natural', '2021-10-01', NULL, NULL);
INSERT INTO `trajects` VALUES (7, 'EA Regulier risico 3', 12, 3, 7, NULL, 2, NULL, NULL, '2021-07-01', '2021-06-30', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2020-11-12 07:49:40', '2021-10-06 09:30:25', '2021-10-06 09:30:25', 'natural', '2020-07-01', 'Evaluatie - 01-07-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (8, 'EA Strategisch risico 1', 12, 3, 4, NULL, 2, NULL, NULL, '2021-12-01', NULL, '2021-08-01', '2021-10-01', '2021-10-14', NULL, NULL, NULL, 200, NULL, NULL, '2020-11-12 07:51:18', '2021-10-06 09:25:54', '2021-10-06 09:25:54', 'natural', '2020-12-01', 'Test - 06-10-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (9, 'Idee-traject', 12, 3, 3, NULL, 2, NULL, NULL, '2022-06-01', '2022-05-31', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2020-11-12 07:56:16', '2021-10-06 09:30:34', '2021-10-06 09:30:34', 'natural', '2021-06-01', NULL, NULL);
INSERT INTO `trajects` VALUES (10, 'test', 6, 2, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-11-12 10:45:24', '2020-11-15 12:39:55', '2020-11-15 12:39:55', NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (11, 'Lange termijn test', 12, 3, 16, NULL, 2, NULL, NULL, '2023-11-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2020-11-18 10:07:12', '2021-10-06 09:30:42', '2021-10-06 09:30:42', 'natural', '2022-11-01', NULL, NULL);
INSERT INTO `trajects` VALUES (12, 'test', 12, 2, 7, NULL, 1, NULL, NULL, '2020-12-01', '2020-11-20', NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2020-11-18 10:23:10', '2021-10-06 09:30:54', '2021-10-06 09:30:54', NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (15, 'ICT-applicaties Sociaal Domein gemeente Gooise Meren', 12, 3, 6, NULL, 2, NULL, NULL, '2021-09-30', NULL, '2021-10-04', '2021-11-04', '2021-11-18', NULL, NULL, NULL, 200, NULL, NULL, '2021-01-26 14:48:38', '2021-11-03 14:55:06', NULL, 'natural', '2020-09-30', NULL, NULL);
INSERT INTO `trajects` VALUES (16, 'Testtraject 10', 6, 2, 7, NULL, 1, NULL, NULL, '2021-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2021-01-27 15:52:34', '2021-10-06 09:31:06', '2021-10-06 09:31:06', 'natural', '2020-07-01', NULL, NULL);
INSERT INTO `trajects` VALUES (17, 'Kantoorartikelen', 12, 3, 9, 21, 18, 27, NULL, '2021-11-04', NULL, NULL, NULL, NULL, '2021-08-30', NULL, NULL, 50, NULL, NULL, '2021-04-28 12:45:01', '2021-11-04 14:14:03', NULL, 'natural', '2020-11-04', NULL, NULL);
INSERT INTO `trajects` VALUES (18, 'Inkoop tijdschriften en abonnementen', 8, 2, 3, NULL, 1, NULL, NULL, '2022-04-01', '2022-02-28', '2021-10-05', NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2021-04-28 12:46:02', '2021-10-19 11:03:27', NULL, 'natural', '2021-08-01', NULL, 2);
INSERT INTO `trajects` VALUES (19, 'Accountantsdienstverlening', 12, 3, 2, NULL, NULL, NULL, NULL, '2025-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-04-28 12:47:39', '2021-04-28 12:47:39', NULL, 'natural', '2024-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (20, 'Schoonmaakdienstverlening', 12, 3, 2, NULL, NULL, NULL, NULL, '2026-12-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-04-28 12:48:16', '2021-04-28 12:48:16', NULL, 'natural', '2026-12-01', NULL, NULL);
INSERT INTO `trajects` VALUES (21, 'Inkoopadvies', 12, 3, 4, NULL, 1, NULL, NULL, '2021-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-04-28 12:57:38', '2021-10-07 13:20:11', NULL, 'natural', '2020-09-01', NULL, NULL);
INSERT INTO `trajects` VALUES (22, 'Testtraject Rens', 12, 3, 3, 21, NULL, 1, NULL, '2022-09-01', '2021-08-31', '2021-07-30', '2021-09-30', '2021-09-30', '2021-08-30', '2021-09-09', NULL, 200, NULL, NULL, '2021-05-18 12:54:48', '2021-09-14 07:08:06', '2021-09-14 07:08:06', 'natural', '2021-09-01', 'Gewoon een opmerking over dit traject. Let op: snel starten! - 18-05-2021 Boudewijn van Immerseel', 2);
INSERT INTO `trajects` VALUES (23, 'Inkoopadvies test', 3, 4, 16, 21, 21, 17, NULL, '2022-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, NULL, NULL, '2021-05-18 13:08:50', '2021-10-06 09:12:46', '2021-10-06 09:12:46', 'natural', '2021-03-01', 'Test vermelding - 18-06-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (24, 'Verlenging catering', 1, 1, 3, NULL, 21, 2, NULL, '2022-05-01', '2022-04-30', NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, '2021-05-18 13:26:30', '2021-10-07 11:47:59', NULL, 'natural', '2022-04-01', NULL, 1);
INSERT INTO `trajects` VALUES (25, 'Inkoop test', 3, 1, 3, 21, 21, NULL, NULL, '2021-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, NULL, NULL, '2021-05-20 07:49:48', '2021-05-20 07:50:06', '2021-05-20 07:50:06', 'natural', NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (26, 'Inkoop wagenpark', 8, 3, 3, 16, 21, 20, 15, '2022-07-01', '2022-06-30', '2022-04-08', '2022-05-20', '2022-06-10', '2022-05-01', NULL, NULL, 200, NULL, NULL, '2021-05-31 11:27:45', '2021-10-07 13:11:05', NULL, 'natural', '2021-11-01', NULL, 1);
INSERT INTO `trajects` VALUES (27, 'Inkoop kerstgeschenken', 5, 2, 4, 16, 21, 20, 15, '2021-11-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 150, NULL, NULL, '2021-06-02 09:42:14', '2021-10-06 12:13:18', NULL, 'natural', '2021-06-25', NULL, 1);
INSERT INTO `trajects` VALUES (28, 'Inkoop ICT Hardware', 12, 3, 3, 18, 21, NULL, NULL, '2021-12-31', '2021-12-30', '2021-07-15', '2021-09-15', '2021-10-22', '2021-07-22', '2021-08-05', NULL, 175, NULL, NULL, '2021-06-07 07:02:28', '2021-08-17 13:44:37', NULL, 'natural', '2020-12-31', 'Test - 17-08-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (29, 'Inkoop test', 6, 2, 3, 16, 21, NULL, NULL, '2022-01-15', '2021-07-14', NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2021-06-07 07:12:55', '2021-09-14 07:54:52', '2021-09-14 07:54:52', 'natural', '2021-07-15', NULL, NULL);
INSERT INTO `trajects` VALUES (30, 'Inkoop laptops', 6, 2, 5, NULL, 21, 20, NULL, '2021-11-15', NULL, '2021-08-02', '2021-09-13', '2021-10-01', '2021-08-17', '2021-08-27', NULL, 80, NULL, NULL, '2021-06-09 12:39:49', '2021-10-07 13:18:11', NULL, 'natural', '2021-05-15', NULL, NULL);
INSERT INTO `trajects` VALUES (31, 'TEST BRM -', 6, 2, 2, NULL, NULL, NULL, NULL, '2021-06-23', '2021-06-21', '2021-06-15', '2021-07-26', '2021-08-30', '2021-06-29', NULL, NULL, 100, NULL, NULL, '2021-06-15 14:02:26', '2021-10-06 09:31:20', '2021-10-06 09:31:20', 'natural', '2020-12-23', NULL, NULL);
INSERT INTO `trajects` VALUES (32, 'Test project BRM (7 april)', 12, 3, 4, 16, NULL, 17, 15, '2022-01-01', '2021-12-31', '2021-07-01', '2021-11-01', '2021-12-06', '2021-08-16', NULL, NULL, 200, NULL, NULL, '2021-06-15 14:11:42', '2021-07-05 13:00:00', '2021-07-05 13:00:00', 'natural', '2021-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (33, 'Test', 6, 2, 3, 16, NULL, 17, 15, '2021-08-31', '2021-09-01', '2021-06-15', '2021-07-19', '2021-07-01', '2021-06-28', NULL, NULL, 100, NULL, NULL, '2021-06-15 14:16:53', '2021-07-05 13:00:38', '2021-07-05 13:00:38', 'natural', '2021-03-03', NULL, NULL);
INSERT INTO `trajects` VALUES (34, 'Inkoop Doppers', 10, 3, 4, NULL, 21, NULL, NULL, '2022-02-01', NULL, '2021-06-21', NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-06-16 14:44:41', '2021-06-22 07:49:52', NULL, 'natural', '2021-04-01', 'Rens - 16-06-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (35, 'Bedrijfskleding', 6, 2, 3, 16, 27, NULL, 15, '2022-01-01', '2021-12-31', NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2021-06-18 07:38:07', '2021-10-06 12:55:37', NULL, 'natural', '2021-07-01', NULL, 1);
INSERT INTO `trajects` VALUES (36, 'Inkoop goodies', 12, 3, 3, NULL, 21, 18, NULL, '2022-08-01', '2022-07-30', NULL, NULL, NULL, NULL, NULL, NULL, 160, NULL, NULL, '2021-06-21 12:14:00', '2021-07-01 11:55:43', '2021-07-01 11:55:43', 'natural', '2021-10-01', NULL, NULL);
INSERT INTO `trajects` VALUES (37, 'Test idee', 12, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-08-03 14:17:32', '2021-08-04 14:39:21', '2021-08-04 14:39:21', NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (38, 'Test 123', 6, 3, 3, NULL, 21, NULL, NULL, '2022-09-30', '2022-09-29', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-08-24 12:18:37', '2021-10-06 09:12:59', '2021-10-06 09:12:59', NULL, '2021-09-30', NULL, NULL);
INSERT INTO `trajects` VALUES (39, 'Complex test', 12, 15, 3, NULL, NULL, NULL, NULL, '2022-08-25', NULL, '2021-10-22', '2021-12-24', '2022-03-26', '2021-11-12', '2021-11-25', NULL, 240, NULL, NULL, '2021-08-26 09:44:56', '2021-10-06 09:31:32', '2021-10-06 09:31:32', NULL, '2021-08-25', 'Test - 26-08-2021 Rens Nibbeling', 1);
INSERT INTO `trajects` VALUES (40, 'Test verlengen', 6, 11, 13, NULL, NULL, NULL, NULL, '2022-05-01', '2022-04-30', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-08-30 13:08:45', '2021-10-06 09:31:45', '2021-10-06 09:31:45', NULL, '2021-11-01', NULL, 2);
INSERT INTO `trajects` VALUES (41, 'Test intern', 12, 99, 3, NULL, 21, NULL, NULL, '2022-01-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, NULL, '2021-08-31 07:59:42', '2021-09-01 11:29:01', '2021-09-01 11:29:01', NULL, '2021-01-21', NULL, NULL);
INSERT INTO `trajects` VALUES (42, 'Non-inkoop traject', 12, 99, 3, NULL, 21, NULL, NULL, '2022-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 208, NULL, NULL, '2021-09-06 07:59:07', '2021-10-06 09:19:17', NULL, NULL, '2021-01-01', NULL, 1);
INSERT INTO `trajects` VALUES (43, 'Onderhoud wagenpark', 10, 8, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 175, NULL, NULL, '2021-09-14 07:03:48', '2021-09-14 07:04:36', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (44, 'Test Test', 10, 3, 2, NULL, NULL, NULL, NULL, '2023-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-09-14 08:55:37', '2021-09-14 10:13:41', '2021-09-14 10:13:41', NULL, '2022-03-01', 'Traject is verwijderd omdat het een testtraject was. - 14-09-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (45, 'Onderhoud gemeentehuis', 12, 3, 3, NULL, 21, 20, NULL, '2022-09-15', NULL, '2022-01-15', '2022-03-25', '2022-05-25', '2022-02-11', '2022-02-25', NULL, 200, NULL, NULL, '2021-09-16 08:14:24', '2021-10-18 13:03:06', NULL, 'good', '2021-09-15', 'Rens Nibbeling gekoppeld als inkoper. - 16-09-2021 Rens Nibbeling\nTeam Procureplan gekoppeld als meelezer - 16-09-2021 Rens Nibbeling\nIk ben akkoord - 18-10-2021 Boudewijn van Immerseel', 1);
INSERT INTO `trajects` VALUES (46, 'Inkooptraject lange termijn', 12, 3, 16, NULL, NULL, NULL, NULL, '2022-09-28', '2022-09-27', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-09-27 10:22:09', '2021-09-27 10:29:13', '2021-09-27 10:29:13', NULL, '2021-09-28', NULL, NULL);
INSERT INTO `trajects` VALUES (47, 'Inkooptraject lange termijn 2', 12, 3, 16, NULL, 21, NULL, NULL, '2023-01-28', '2023-01-27', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-09-27 10:25:13', '2021-09-27 10:28:54', '2021-09-27 10:28:54', NULL, '2022-01-28', NULL, NULL);
INSERT INTO `trajects` VALUES (48, 'Lange termijn test123', 12, 3, 2, NULL, NULL, NULL, NULL, '2024-01-01', '2023-12-31', NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-09-27 10:26:44', '2021-10-06 09:31:59', '2021-10-06 09:31:59', NULL, '2023-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (49, 'Testtraject lange termijn', 12, 3, 2, NULL, NULL, NULL, NULL, '2023-09-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-09-27 10:30:47', '2021-10-06 09:32:07', '2021-10-06 09:32:07', NULL, '2022-09-28', NULL, NULL);
INSERT INTO `trajects` VALUES (50, 'Inkoop laptops', 3, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, '2021-10-06 12:38:01', '2021-10-06 12:38:01', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (51, 'Inkoop bloemen', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2021-10-06 12:38:47', '2021-10-06 12:38:47', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (52, 'Inkoop CRM systeem', 18, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 450, NULL, NULL, '2021-10-06 12:39:49', '2021-10-06 12:39:49', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (53, 'Non-inkoop traject 2022', 12, 99, 4, NULL, 21, NULL, NULL, '2023-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 208, NULL, NULL, '2021-10-06 12:49:01', '2021-10-06 12:49:01', NULL, NULL, '2022-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (54, 'Inkoop catering', 12, 3, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, '2021-10-06 12:59:36', '2021-10-06 12:59:53', '2021-10-06 12:59:53', NULL, NULL, NULL, NULL);
INSERT INTO `trajects` VALUES (55, 'Non-inkoop traject', 12, 99, 4, NULL, 18, NULL, NULL, '2022-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, '2021-10-06 14:33:43', '2021-10-06 14:33:43', NULL, NULL, '2021-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (56, 'Non-inkoop traject', 12, 99, 4, NULL, 27, NULL, NULL, '2022-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, '2021-10-06 14:38:47', '2021-10-06 14:38:47', NULL, NULL, '2021-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (57, 'Non-inkoop traject 2022', 12, 99, 4, NULL, 27, NULL, NULL, '2023-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, '2021-10-06 14:51:20', '2021-10-11 09:56:15', NULL, NULL, '2022-01-01', 'test - 11-10-2021 Jasper van Tetering', NULL);
INSERT INTO `trajects` VALUES (58, 'Non-inkoop traject 2022', 12, 99, 4, NULL, 18, NULL, NULL, '2023-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, '2021-10-06 14:54:49', '2021-10-06 14:54:49', NULL, NULL, '2022-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (59, 'Non-inkoop traject 2022', 12, 99, 4, NULL, 1, NULL, NULL, '2023-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, '2021-10-06 14:57:12', '2021-10-06 14:57:12', NULL, NULL, '2022-01-01', NULL, NULL);
INSERT INTO `trajects` VALUES (60, 'Non-inkoop traject', 12, 99, 4, NULL, 1, NULL, NULL, '2022-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, '2021-10-06 14:59:12', '2021-10-19 16:43:38', NULL, NULL, '2021-01-01', '- - 18-10-2021 Rens Nibbeling\n- - 19-10-2021 Rens Nibbeling', NULL);
INSERT INTO `trajects` VALUES (61, 'Test JASPER', 12, 3, 3, 18, 4, NULL, NULL, '2022-06-30', NULL, '2022-02-01', '2022-04-01', '2022-06-01', NULL, NULL, NULL, 200, NULL, NULL, '2021-11-02 16:55:45', '2021-11-04 09:32:13', NULL, 'excellent', '2021-06-30', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for user_agendas
-- ----------------------------
DROP TABLE IF EXISTS `user_agendas`;
CREATE TABLE `user_agendas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `week` int(11) NOT NULL,
  `hours` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `year` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_agendas
-- ----------------------------
BEGIN;
INSERT INTO `user_agendas` VALUES (8, 3, 11, 1, '2021-03-18 15:35:17', '2021-03-18 15:35:17', 2021);
INSERT INTO `user_agendas` VALUES (9, 3, 12, 31, '2021-03-18 15:35:28', '2021-03-18 15:35:28', 2021);
INSERT INTO `user_agendas` VALUES (10, 3, 14, 0, '2021-03-18 15:49:00', '2021-03-18 15:49:00', 2021);
INSERT INTO `user_agendas` VALUES (17, 2, 39, 0, '2021-03-23 13:16:05', '2021-03-23 13:16:05', 2021);
INSERT INTO `user_agendas` VALUES (22, 2, 29, 0, '2021-06-01 12:22:47', '2021-06-01 12:22:47', 2021);
INSERT INTO `user_agendas` VALUES (23, 2, 37, 0, '2021-06-01 12:22:56', '2021-06-01 12:22:56', 2021);
INSERT INTO `user_agendas` VALUES (24, 2, 52, 0, '2021-06-01 12:23:07', '2021-06-01 12:23:07', 2021);
INSERT INTO `user_agendas` VALUES (31, 2, 26, 0, '2021-06-01 12:26:38', '2021-06-01 12:26:38', 2021);
INSERT INTO `user_agendas` VALUES (90, 1, 35, 0, '2021-08-04 08:24:58', '2021-08-04 08:24:58', 2021);
INSERT INTO `user_agendas` VALUES (94, 1, 38, 10, '2021-08-04 08:30:49', '2021-08-04 08:30:49', 2021);
INSERT INTO `user_agendas` VALUES (95, 1, 34, 0, '2021-08-04 08:31:08', '2021-08-04 08:31:08', 2021);
INSERT INTO `user_agendas` VALUES (96, 1, 45, 10, '2021-08-04 08:31:15', '2021-08-04 08:31:15', 2021);
INSERT INTO `user_agendas` VALUES (97, 1, 35, 10, '2021-08-04 08:31:33', '2021-08-04 08:31:33', 2021);
INSERT INTO `user_agendas` VALUES (98, 1, 38, 0, '2021-08-04 08:31:51', '2021-08-04 08:31:51', 2021);
INSERT INTO `user_agendas` VALUES (99, 1, 33, 0, '2021-08-04 08:32:08', '2021-08-04 08:32:08', 2021);
INSERT INTO `user_agendas` VALUES (100, 1, 32, 9, '2021-08-04 08:45:47', '2021-08-04 08:45:47', 2021);
INSERT INTO `user_agendas` VALUES (101, 1, 34, 7, '2021-08-04 08:49:06', '2021-08-04 08:49:06', 2021);
INSERT INTO `user_agendas` VALUES (102, 1, 4, 20, '2021-08-04 08:50:05', '2021-08-04 08:50:05', 2021);
INSERT INTO `user_agendas` VALUES (103, 1, 20, 20, '2021-08-04 08:51:22', '2021-08-04 08:51:22', 2021);
INSERT INTO `user_agendas` VALUES (110, 1, 31, 1, '2021-08-23 14:43:55', '2021-08-23 14:43:55', 2021);
INSERT INTO `user_agendas` VALUES (116, 21, 6, 0, '2021-10-06 10:11:32', '2021-10-06 10:11:32', 2022);
INSERT INTO `user_agendas` VALUES (118, 21, 7, 16, '2021-10-06 10:12:23', '2021-10-06 10:12:23', 2022);
INSERT INTO `user_agendas` VALUES (120, 21, 41, 0, '2021-10-13 06:59:45', '2021-10-13 06:59:45', 2021);
INSERT INTO `user_agendas` VALUES (121, 27, 44, 0, '2021-10-18 11:55:20', '2021-10-18 11:55:20', 2021);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `start_contract` date DEFAULT NULL,
  `end_contract` date DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sso_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sso_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_expires_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `confirm` int(11) DEFAULT NULL,
  `last_password_change` timestamp NULL DEFAULT NULL,
  `active` int(255) DEFAULT NULL,
  `loginCount` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_sso_id_unique` (`sso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Radek', 'Dolewa', 0, '2020-11-10', '2020-11-10', 'rdolewa@procurance.nl', NULL, '$2y$10$jJpJ8jwspfC3oTV7lFfB.ejw5pBZJNmG97FKX03jiYUuPHcfuQ0aO', NULL, NULL, NULL, NULL, NULL, '2020-11-10 08:50:57', '2021-01-12 09:07:41', 1, '2020-11-18 10:03:07', 1, 26);
INSERT INTO `users` VALUES (2, 'Boudewijn', 'van Immerseel', 0, '2020-11-10', '2020-11-10', 'boudewijn@procurebright.nl', NULL, '$2y$10$jJpJ8jwspfC3oTV7lFfB.ejw5pBZJNmG97FKX03jiYUuPHcfuQ0aO', NULL, NULL, NULL, NULL, 'jlmx7lFxQ5zv2UlfrOdAIu0girYnabA0eLuETcirUdpkcRgPxKKPBL1LlppA', '2020-11-10 08:50:57', '2021-10-06 12:51:43', 1, '2020-11-18 10:04:21', 1, 20);
INSERT INTO `users` VALUES (3, 'Boudewijn', 'Test Uitnodigingsmail', 0, '2021-03-18', '2021-03-18', 'boudewijnvanimmerseel@gmail.com', NULL, '$2y$10$6KoWlZ88Tj1PfFuYmkU0kOQouTSd.1.c3z6Pd8Wq3RNGMVmH6wit.', NULL, NULL, NULL, NULL, '7tsbOjhKmcv09D6FV4TujnDTfYQlCnMbLbW8jbfxPOOCRd3WgJkVUyWRxwUo', '2020-11-15 12:34:53', '2021-10-06 13:46:57', 1, '2020-11-23 16:31:47', 0, 1);
INSERT INTO `users` VALUES (4, 'Test', 'Inkoper', 0, '2020-11-25', '2020-11-25', 'info@procureplan.nl', NULL, '$2y$10$29nGVN0TDaTX8a52nvE1hOrtmn2FrjsIinGCWaRYl7qd5Jr7zLHhq', NULL, NULL, NULL, NULL, NULL, '2020-11-25 10:02:17', '2021-11-04 09:32:30', 1, '2021-07-07 07:36:21', 0, NULL);
INSERT INTO `users` VALUES (15, 'Test', 'Jurist', NULL, '2020-11-25', '2020-11-25', 'dev@procureplan.nl', NULL, '$2y$10$dtQGVCpePf2kRKTTHgHVvewNV.8GyopAs0bZzp0.Jg/9Hfpy6ricq', NULL, NULL, NULL, NULL, NULL, '2020-11-25 10:03:00', '2021-08-25 09:43:21', 1, '2021-06-07 07:45:25', 1, NULL);
INSERT INTO `users` VALUES (16, 'Test', 'Contractmanager', NULL, '2020-11-25', '2020-11-25', 'bvanimmerseel@procurance.nl', NULL, '$2y$10$EYjAUHO9NU.5Y4iCGoErt.peaLlyqgyGG8bF2XIiO6ecck1kzV3Y2', NULL, NULL, NULL, NULL, NULL, '2020-11-25 10:03:22', '2020-11-25 10:03:22', NULL, NULL, 1, NULL);
INSERT INTO `users` VALUES (17, 'Test', 'Meelezer', NULL, '2020-11-25', '2020-11-25', 'servicedesk@procureplan.nl', NULL, '$2y$10$mk033UFyx6FQsnzrHDIcf.y1h/jZLUZONUvfv0rpP5RsoDI.X3i1y', NULL, NULL, NULL, NULL, NULL, '2020-11-25 10:04:53', '2020-11-25 10:04:53', NULL, NULL, 1, NULL);
INSERT INTO `users` VALUES (18, 'Bart', 'Berkel', 0, '2021-02-01', '2021-02-01', 'bberkel@procurance.nl', NULL, '$2y$10$1oW60bi9KRNP9tXH44c69uza94QBsQadDRedN2ZZY75JpkU.5irbS', NULL, NULL, NULL, NULL, 'Waud9ZJKE0n2X1wxghVgEduxEbkUGqieWxXUnGXuknPvWzaQtaj4S2zPMtNt', '2021-02-01 15:53:01', '2021-10-06 12:52:47', 1, '2021-02-12 09:58:34', 1, NULL);
INSERT INTO `users` VALUES (19, 'Jennifer', 'Test', 0, '2021-02-05', '2021-02-05', 'jennifer@procurance.nl', NULL, '$2y$10$lp6IE68fPiQxgxDcfUWJ9.65KLizcthyMsoo58h5C5BrTmBOyw4gy', NULL, NULL, NULL, NULL, NULL, '2021-02-05 13:47:21', '2021-08-17 08:55:13', NULL, NULL, 0, NULL);
INSERT INTO `users` VALUES (20, 'Team', 'Procureplan', 0, '2021-02-18', '2021-02-18', 'team@procureplan.nl', NULL, '$2y$10$j.j49roUJJUz0N1Du0XdvOiuF5AvyjxKewA5PGwiUMZWPynaJ/Aqi', NULL, NULL, NULL, NULL, '8quMVpaCiAfM74KyRDwx5OEfqjFO8m5v7MHraDDDYUgjf7jE6U4ACkjZvSkp', '2021-02-18 09:57:21', '2021-03-04 10:11:45', 1, '2021-02-18 09:58:01', 1, NULL);
INSERT INTO `users` VALUES (21, 'Rens', 'Nibbeling', 28, '2021-05-17', '2021-05-17', 'rens@procurebright.nl', NULL, '$2y$10$5Y9E6xgnxVlpmZEp4oGkKOSQMuCnB7rZTSTMG2jHD8gKYR7ESvIZK', NULL, NULL, NULL, NULL, 'Fx3FA1GBC0qccZQchg6dHYnFwtTAkD7RcaDDDvhdXiJNQ7GAqnGfYvcW7PNY', '2021-05-17 09:36:13', '2021-10-14 09:29:17', 1, '2021-05-17 10:06:58', 1, 100);
INSERT INTO `users` VALUES (23, 'Joost', 'Tester', NULL, '2021-06-01', '2021-06-01', 'j.van.tester@procureplan.nl', NULL, '$2y$10$2j7Q1S40CgLr7kKooCUmmerVSxJsTpvvhYFYAEcPCGdO026ZSZZLm', NULL, NULL, NULL, NULL, '8DgB4OJyEQ5xhS3C2G8oxpQLRicy3ZTGuXUYEq1UhDCuss7dunJ2IHBTOPrN', '2021-06-01 13:46:19', '2021-06-23 07:09:45', 1, '2021-06-01 14:21:17', 0, NULL);
INSERT INTO `users` VALUES (25, 'Koen', 'Test', 40, '2021-08-16', '2021-08-16', 'koen.test@procureplan.nl', NULL, '$2y$10$kxqOeGLfSiNWyw/7C02EDu/SUOZdB0VdonQ/GDWWI7QAsdviGFzeO', NULL, NULL, NULL, NULL, 'eCHouzHIkmuXRSWnuukLopscVv6X1EIOWjonjy0JcmEcicP4vqB2dK9XQYsP', '2021-08-16 14:40:55', '2021-08-25 11:21:19', 1, '2021-08-16 14:50:45', 0, 2);
INSERT INTO `users` VALUES (26, 'Test', 'Account', 0, '2021-09-14', '2021-09-14', 'testaccount@procureplan.nl', NULL, '$2y$10$UoJDLXcWpCIH4moogGGiwe5ukTQ3t.T87ZIG5ul727tkLXHYrD1MC', NULL, NULL, NULL, NULL, NULL, '2021-09-14 08:41:37', '2021-11-04 09:34:50', 1, '2021-09-14 09:39:49', 1, NULL);
INSERT INTO `users` VALUES (27, 'Jasper', 'van Tetering', 16, '2021-10-04', '2021-10-04', 'jasper@procurebright.nl', NULL, '$2y$10$4jM3iGjoCfry.s313Jjtd.HxGNZGmjZsdaB.IHRQVZaIRRGXXPd/.', NULL, NULL, NULL, NULL, 'qhVDfMnSZg7vfBDhFVlSy6vDnjkf7HAFRHsMWHhJUvkcf1IQoJDyjIoL973v', '2021-10-04 11:29:34', '2021-10-13 07:04:42', 1, '2021-10-04 11:31:53', 1, 43);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
